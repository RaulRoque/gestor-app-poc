// index.js
module.exports = () => {
  const data = {
    users: { totalRecords: 260, users: [] },
    years: getYears(),
    levels: getLevels(),
    grades: getGrade(),
    schoolCollections: getCollections(),
    schoolClasses: getSchoolClasses(),
  }
  // Create 1000 users
  for (let i = 0; i < 10; i++) {
    const relatedUsers = getRelatedUsers(i)
    data.users.users.push({
      id: i,
      name: `user${i}`,
      username: `username${i}`,
      email: `useremail${i}@gmail.com`,
      profiles: ['Aluno'],
      disciplines: [
        { id: 1, name: 'Português' },
        { id: 2, name: 'Geografia' },
        { id: '3', name: 'História' },
      ],
      schoolClasses: [
        {
          id: i,
          description: `Turma ${i}`,
        },
      ],
      relatedUsers: relatedUsers,
      status: 'Active',
      createdAt: '10/10/2010',
    })
  }
  return data
}
function getSchoolClasses() {
  const schoolClasses = { totalRecords: 260, schoolClasses: [] }
  for (let i = 0; i < 10; i++) {
    const relatedUsers = getRelatedUsers(i)
    schoolClasses.schoolClasses.push({
      schoolClass: {
        id: i,
        description: `Turma ${i}`,
        idSchool: 3644977,
        creationDate: '2018-05-07',
        status: 'Active',
      },
      grade: {
        id: '100631',
        description: '1\u00ba ANO',
      },
      period: {
        id: 4,
        year: 2018,
        status: 1,
      },
      level: {
        id: '7',
        description: 'Ensino Médio',
      },
      totalMembers: 0,
      collections: [
        {
          id: 916,
          description: 'Regular - Sistema Anglo',
        },
        {
          id: 917,
          description: 'Complementar - Sistema Anglo',
        },
      ],
      relatedUsers: relatedUsers,
      status: 'Active',
      createdAt: '10/10/2010',
    })
  }
  return schoolClasses
}
function getRelatedUsers(i) {
  return [
    {
      id: i,
      description: `Turma ${i}`,
    },
  ]
}
function getYears() {
  return [
    { description: '2018', value: '2018' },
    { description: '2018', value: '2018' },
    { description: '2017', value: '2016' },
  ]
}

function getGrade() {
  return [
    { description: '1 ano', value: '1' },
    { description: '2 ano', value: '2' },
    { description: '3 ano', value: '3' },
  ]
}

function getLevels() {
  return [
    { description: 'Educação Infantil', value: '3' },
    { description: 'Ensino Fundamental 1', value: '5' },
    { description: 'Ensino Fundamental 2', value: '6' },
    { description: 'Ensino Médio', value: '7' },
    { description: 'Pré-Vestibular', value: '14' },
  ]
}

function getCollections() {
  return [
    { label: 'Regular - Sistema Anglo', value: 916 },
    { label: 'Complementar - Sistema Anglo', value: 917 },
    { label: 'Bienal - Sistema Anglo', value: 918 },
    { label: 'Teste - Sistema Anglo', value: 919 },
  ]
}
