import { BrowserRouter, Route } from 'react-router-dom'
import * as React from 'react';

import { getToken } from 'src/utils/token'
import App from '../App'

interface IProps {
  component: any;
  match?: any;
}

const PrivateRoute: React.SFC<IProps> = ({ component: Component, ...rest }, privateProps: IProps) => (
  <Route
    {...rest}
    render={props => {
      return getToken() ? (
        <Component {...props} />
      ) : (
        // handleNotAuthenticated(props.match.path)
        <Component {...props} />
      )
    }}
  />
)

const Router = () => {
  return (
    <BrowserRouter>
      <App />
    </BrowserRouter>
  )
}

export default Router
