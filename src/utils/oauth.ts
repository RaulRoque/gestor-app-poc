import * as ClientOAuth2 from 'client-oauth2'

const getOauthClient = path => {
  return new ClientOAuth2({
    accessTokenUri: process.env.REACT_APP_COMMUNICATION_ACCESS_TOKEN_URI,
    authorizationUri: process.env.REACT_APP_COMMUNICATION_AUTHORIZATION_URI,
    clientId: process.env.REACT_APP_COMMUNICATION_CLIENT_ID,
    clientSecret: process.env.REACT_APP_COMMUNICATION_CLIENT_SECRET,
    redirectUri: `${
      process.env.REACT_APP_COMMUNICATION_REDIRECT_URI
    }?redirectTo=${path}`,
    scopes: [],
  })
}

export { getOauthClient }
