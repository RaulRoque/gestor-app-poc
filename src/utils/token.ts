const getToken = () => {
  const value = '; ' + document.cookie
  const parts = value.split('; authenticated_token=')

  if (parts.length === 2) {
    return parts
      .pop()
      .split(';')
      .shift()
  }
  return '1'
}

function setToken(token) {
  document.cookie = 'authenticated_token=' + (token || '') + '; path=/'
}

export { getToken, setToken }
