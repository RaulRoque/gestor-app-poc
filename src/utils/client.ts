import PlurallClient from 'plurall-client'
import { getToken } from 'src/utils/token'

class Client {
  public client: any
  public getProfile: any
  public getMenus: any

  constructor(config) {
    this.client = new PlurallClient({
      accessToken: getToken(),
      apiURL: process.env.REACT_APP_COMMUNICATION_API_URL,
      profileURL: process.env.REACT_APP_COMMUNICATION_API_PROFILE_URL,
    })

    this.getProfile = this.client.getProfile
    this.getMenus = this.client.getMenu
  }

  public getHello = () => this.client.get('/hello')
}

export default Client
