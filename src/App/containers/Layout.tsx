import * as React from 'react';

// import { getOauthClient } from 'utils/oauth'
import { setToken } from 'src/utils/token'

class Layout extends React.Component {

  // tslint:disable-next-line:no-empty
  public async componentDidMount() {}

  public handleLogout = () => {
    setToken('')
    // const oauth = getOauthClient()
  }

  public render() {
    return (
      <div>
        <div style={{ minHeight: 'calc(100vh - 85px - 59px)' }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Layout
