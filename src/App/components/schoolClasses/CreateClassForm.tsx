import * as React from 'react';

import { Text, Title, Slug } from 'plurall-texts'
import { Input } from 'plurall-form'
import Dropdown from 'plurall-dropdown'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import FilterService from '../../services/FilterServices'
import Chip from '../common/Chip'
import { collectFields } from 'graphql/execution/execute';

interface IState {
  className?: any
  qtdUsers?: any
  selectedYear?: any
  selectedGrade?: any
  collectionsSelectedes?: any
  selectedLevel?: any
  indexClass?: any
  levels?: any
  grades?: any
  collections?: any
}

interface IProps {
  onQtdUsers?: boolean
  onChangeFormDataClass?: any
  onChangeFormInvalid?: any
  dataClassName?: string
  dataQtdUsers?: any
  dataYear?: string
  dataLevel?: any
  dataGrade?: any
  dataCollections?: any
  indexClass?: any
  year?: any
  className?: any
  qtdUsers?: any
  level?: any
  grade?: any
  collections?: any
}

export default class CreateClassForm extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      indexClass: '',
      className: '',
      selectedYear: '',
      qtdUsers: '',
      selectedLevel: '',
      selectedGrade: '',
      collectionsSelectedes: [],
      levels: [{ label: 'Segmento', value: '' }],
      grades: [{ label: 'Série', value: '' }],
      collections: [{ label: 'Selecione', value: '' }],
    }
  }

  public bindHandlers() {
    this.handleChangeClassName = this.handleChangeClassName.bind(this)
    this.handleChangeQtdUsers = this.handleChangeQtdUsers.bind(this)
    this.handleChangeYear = this.handleChangeYear.bind(this)
    this.handleChangeLevel = this.handleChangeLevel.bind(this)
    this.handleChangeGrade = this.handleChangeGrade.bind(this)
    this.removeCollectionsSelectedes = this.removeCollectionsSelectedes.bind(
      this,
    )
    this.handleChangeCollection = this.handleChangeCollection.bind(this)
  }

  public componentDidUpdate() {
    const classData = {
      indexClass: this.props.indexClass,
      className: this.state.className,
      qtdUsers: this.state.qtdUsers,
      year: this.state.selectedYear,
      level: this.state.selectedLevel,
      grade: this.state.selectedGrade,
      collections: this.state.collectionsSelectedes,
    }
    if (this.validateDataClass(classData)) {
      classData['invalid'] = false
      this.props.onChangeFormDataClass(classData)
    } else {
      classData['invalid'] = true
      this.props.onChangeFormInvalid(classData)
    }
  }

  public componentDidMount() {
    if (this.props.year) {
      this.setState({ indexClass: this.props.indexClass })
      this.setState({ className: this.props.className })
      this.setState({ qtdUsers: this.props.qtdUsers })
      this.handleChangeYear(this.props.year)
      this.handleChangeLevel(this.props.level)
      this.handleChangeGrade(this.props.grade)
      this.props.collections.map(item => {
        this.state.collectionsSelectedes.push(item)
      })
    }
  }

  public validateDataClass(data) {
    if (
      data.className.length > 0 &&
      data.year.length > 0 &&
      data.level.length > 0 &&
      data.grade.length > 0 &&
      data.collections.length > 0
    ) {
      return true
    } else {
      return false
    }
  }

  public handleChangeClassName(e) {
    this.setState({ className: e.target.value })
  }

  public handleChangeQtdUsers(e) {
    this.setState({ qtdUsers: e.target.value })
  }

  public handleChangeYear(value) {
    this.setState({ selectedYear: value })
    this.getFilters(value, null)
  }

  public handleChangeLevel(value) {
    this.setState({ selectedLevel: value })
    this.getFilters(null, value)
  }

  public handleChangeGrade(value) {
    this.setState({ selectedGrade: value })
    this.getFilters(null, null, value)
  }

  public handleChangeCollection(value) {
    if (value !== '') {
      this.state['collections'].forEach((collection, index) => {
        this.validateCollection(index, collection, value)
      })
    }
  }

  public validateCollection(index, collection, value) {
    if (collection.value === value) {
      if (this.existsCollectionSelected(value) === true) {
        this.removeCollectionsSelectedes({ item: collection })
      } else {
        this.insertCollectionSelected(collection.label, collection.value)
      }
    }
  }

  public existsCollectionSelected(value) {
    return this.state.collectionsSelectedes.some(collectionSelected => {
      return collectionSelected.value === value
    })
  }

  public insertCollectionSelected(label, key) {
    const collections = this.state.collectionsSelectedes
    collections.push({ label: label, value: key })
    this.setState({ collectionsSelectedes: collections.slice() })
  }

  public removeCollectionsSelectedes(obj) {
    return this.state.collectionsSelectedes.some((collection, index) => {
      if (collection.value === obj.item.value) {
        const collectionsChip = this.state.collectionsSelectedes
        collectionsChip.splice(index, 1)
        this.setState({ collectionsSelectedes: collectionsChip.slice() })
      }
    })
  }

  public getYears() {
    return [
      { label: 'Ano Letivo', value: '' },
      { label: '2018', value: '2018' },
      { label: '2017', value: '2017' },
      { label: '2016', value: '2016' },
    ]
  }

  public async getFilters(year, level, grade?) {
    if (year) {
      const data = await FilterService.getLevels(year)
      this.setState({ levels: data['levels'] })
    }
    if (level) {
      const data = await FilterService.getGrades(level)
      this.setState({ grades: data['grades'] })
    }
    if (grade) {
      const data = await FilterService.getSchoolCollections(grade)
      this.setState({ collections: data['schoolCollections'] })
    }
  }

  public render() {
    return (
      <React.Fragment>
        <Row>
          <Grid cols="12 12 6 6">
            <Text>Nome da Turma</Text>
            <Input
              type="text"
              name="className"
              placeholder="Nome"
              onChange={this.handleChangeClassName}
              value={this.state.className}
            />
          </Grid>
          {this.props.onQtdUsers === true && (
            <Grid cols="12 12 6 6">
              <Text>Quantidade de alunos</Text>
              <Input
                type="text"
                name="qtdUsers"
                placeholder="Digite a quantidade"
                onChange={this.handleChangeQtdUsers}
                value={this.state.qtdUsers}
              />
            </Grid>
          )}
        </Row>
        <Row>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%', marginTop: '20px' }}>
              <Text>Ano Letivo</Text>
              <Dropdown
                name="year"
                options={this.getYears()}
                onChange={this.handleChangeYear}
                selected={this.state.selectedYear}
              />
            </div>
          </Grid>
          <Grid cols="12 6 3 3">
            <div style={{ width: '100%', marginTop: '20px' }}>
              <Text>Segmento</Text>
              <Dropdown
                name="levels"
                options={this.state.levels}
                onChange={this.handleChangeLevel}
                selected={this.state.selectedLevel}
              />
            </div>
          </Grid>
          <Grid cols="12 6 3 3">
            <div style={{ width: '100%', marginTop: '20px' }}>
              <Text>Série</Text>
              <Dropdown
                name="grades"
                options={this.state.grades}
                onChange={this.handleChangeGrade}
                selected={this.state.selectedGrade}
              />
            </div>
          </Grid>
          <Grid cols="12 6 4 4">
            <div style={{ width: '100%', marginTop: '20px' }}>
              <Text>Materiais *</Text>
              <Dropdown
                name="collections"
                options={this.state.collections}
                onChange={this.handleChangeCollection}
              />
            </div>
          </Grid>
          {this.state.collectionsSelectedes.length > 0 && (
            <div style={{ marginTop: 20 }}>
              <Grid cols="12 12 12 12">
                <Slug color="#2ABB9C">Materiais Selecionados</Slug>
                <div style={{ width: '100%' }}>
                  <Chip
                    items={this.state.collectionsSelectedes}
                    onClose={this.removeCollectionsSelectedes}
                  />
                </div>
              </Grid>
            </div>
          )}
        </Row>
      </React.Fragment>
    )
  }
}
