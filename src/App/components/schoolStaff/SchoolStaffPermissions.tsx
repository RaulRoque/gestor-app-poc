import * as React from 'react';
import Row from '../../containers/Row'
import Grid from '../../containers/Grid'
import { Text } from 'plurall-texts'
import Button from 'plurall-button/dist/Button'
import newId from 'src/utils/newId'
import { CSSProperties } from 'react';

interface IState {
  levels?: any
  teacherDisciplines?: any
  teacherSchoolClasses?: any
  profiles?: any
}

interface IProps {
  onChange?: any;
  userPermissions?: any
  levels?: any
}

export default class SchoolStaffPermissions extends React.Component<IProps, IState> {
  public levels: any
  public PROFILE_LEVELS = {
    COORDINATOR: 1,
    TEACHER: 2,
  }

  public styleChk: CSSProperties = {
    clear: 'both',
    overflow: 'none',
    background: '#eee',
    borderRadius: '10px',
    padding: '10px',
    margin: '0 10px 10px 0',
  }

  public styleBoxCheck: CSSProperties = {
    float: 'left',
    borderRadius: '10px',
    background: '#fff',
    color: '#000',
    padding: '15px',
    margin: '0 10px 10px 0',
    width: '100%',
    fontSize: '16px',
  }
  public styleLabel: CSSProperties = {
    position: 'relative',
    paddingLeft: '20px',
    cursor: 'pointer',
    fontSize: '16px',
    width: '95%',
  }
  public styleInsideCheckBox: CSSProperties = {
    float: 'left',
    borderRadius: '10px',
    fontSize: '16px',
    border: '5px solid #fff',
    background: 'transparent',
    color: '#000',
    padding: '15px',
    margin: '0 10px 10px 0',
    width: '100%',
    verticalAlign: 'top',
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      profiles: {},
      levels: {
        coordinator: [],
        teacher: [],
      },
      teacherDisciplines: [],
      teacherSchoolClasses: [],
    }
  }

  public bindHandlers() {}

  public renderDisciplinesAndSchoolClasses(level) {
    if (this.state.levels.teacher.indexOf(level.value) > -1) {
      const idBoxDisciplines = newId('disciplineBox')
      const idBoxSchoolClasses = newId('schoolClassesBox')
      const dis = level.disciplines.map((i, index) => {
        const idDiscipline = newId('disciplineItem')
        return (
          <Grid cols="12 12 4 4" key={index}>
            <input
              id={idDiscipline}
              name={i.value}
              type="checkbox"
              onChange={e => this.toggleDisciplineLevel(e, level, i)}
            />
            <label htmlFor={idDiscipline}>{i.label}</label>
          </Grid>
        )
      })
      const schoolClasses = level.schoolClasses.map((i, index) => {
        const idSchoolClass = newId('idSchoolClass')
        return (
          <Grid cols="12 12 4 4" key={index}>
            <input
              id={idSchoolClass}
              name={i.value}
              type="checkbox"
              onChange={e => this.toggleSchoolClassesLevel(e, level, i)}
            />
            <label htmlFor={idSchoolClass}>{i.label}</label>
          </Grid>
        )
      })
      return (
        <div>
          <Button
            type="secondary"
            onClick={e =>
              this.toggleDisciplinesSchoolClassesBox(
                e,
                idBoxDisciplines,
                idBoxSchoolClasses,
              )
            }
          >
            Disciplinas
          </Button>
          <Button
            type="secondary"
            onClick={e =>
              this.toggleDisciplinesSchoolClassesBox(
                e,
                idBoxDisciplines,
                idBoxSchoolClasses,
              )
            }
          >
            Turmas
          </Button>
          <div id={idBoxDisciplines}>
            <Row>{dis}</Row>
          </div>
          <div id={idBoxSchoolClasses} style={{ display: 'none' }}>
            <Row>{schoolClasses}</Row>
          </div>
        </div>
      )
    }

    return undefined;
  }

  public toggleDisciplineLevel(event, level, discipline) {
    const target = event.target
    const disciplines = this.state.teacherDisciplines.slice()
    if (!disciplines[level.value]) {
      disciplines[level.value] = []
    }
    const indexContains = disciplines[level.value].findIndex(el => {
      return el.value === discipline.value
    })
    if (indexContains === -1) {
      disciplines[level.value].push(discipline)
    } else {
      disciplines[level.value].splice(indexContains, 1)
      disciplines[level.value] = disciplines[level.value]
    }
    console.log(disciplines)
    this.setState({
      teacherDisciplines: disciplines,
    })
  }

  public toggleSchoolClassesLevel(event, level, schoolClass) {
    const target = event.target
    const value = target.checked
    // const name = target.name;

    const schoolClasses = this.state.teacherSchoolClasses
    if (!schoolClasses[level.value]) {
      schoolClasses[level.value] = []
    }
    schoolClasses[level.value].push(schoolClass)

    this.setState({
      teacherSchoolClasses: schoolClasses,
    })
  }
  public toggleDisciplinesSchoolClassesBox(e, idBoxDiscipline, idBoxSchoolClasses) {
    const boxd = document.getElementById(idBoxDiscipline)
    boxd.offsetParent === null
      ? (document.getElementById(idBoxDiscipline).style.display = 'block')
      : (document.getElementById(idBoxDiscipline).style.display = 'none')

    const boxsc = document.getElementById(idBoxSchoolClasses)
    boxsc.offsetParent === null
      ? (document.getElementById(idBoxSchoolClasses).style.display = 'block')
      : (document.getElementById(idBoxSchoolClasses).style.display = 'none')
  }
  public renderLevels(
    profile = this.PROFILE_LEVELS.COORDINATOR,
    showDisciplines = false,
    showSchoolClasses = false,
  ) {
    let cols = '12 12 6 6'
    if (profile === this.PROFILE_LEVELS.TEACHER) {
      cols = '12 12 12 12'
    }

    const lev = this.levels.map((i, index) => {
      return (
        <Grid cols={cols} key={index}>
          <div style={this.styleInsideCheckBox}>
            <input
              id={i.value}
              name={i.value}
              type="checkbox"
              style={{ verticalAlign: 'top', marginTop: '6px' }}
              onChange={e => this.handleChangeLevelCheckBox(profile, i, e)}
            />
            <label htmlFor={i.value} style={this.styleLabel}>
              {i.label}
              {profile === this.PROFILE_LEVELS.TEACHER &&
              this.state.profiles.teacher
                ? this.renderDisciplinesAndSchoolClasses(i)
                : ''}
            </label>
          </div>
        </Grid>
      )
    })
    return lev
  }

  public handleChangeLevelCheckBox(profile, level, event) {
    const target = event.target
    const value = target.checked
    // const name = target.name;

    const levels = this.state.levels
    const teacherDisciplines = this.state.teacherDisciplines
    const teacherSchoolClasses = this.state.teacherSchoolClasses

    if (profile === this.PROFILE_LEVELS.COORDINATOR) {
      const indexContains = levels.coordinator.indexOf(level.value)
      if (indexContains) {
        levels.coordinator.push(level.value)
      } else {
        levels.coordinator.splice(indexContains, 1)
        levels.coordinator = levels.coordinator.slice()
      }
      this.setState({
        levels: levels,
      })
    } else {
      const indexContains = levels.teacher.indexOf(level.value)
      if (indexContains) {
        levels.teacher.push(level.value)
      } else {
        levels.teacher.splice(indexContains, 1)
        levels.teacher = levels.teacher.slice()
        teacherDisciplines[level.value] = []
      }
      this.setState({
        levels: levels,
        teacherDisciplines,
        teacherSchoolClasses,
      })
    }
  }
  public handleChangeMainCheckBox(event) {
    const target = event.target
    const value = target.checked
    const name = target.name

    const profiles = this.state.profiles
    profiles[name] = value
    this.setState({
      profiles: profiles,
    })
  }
  public componentDidUpdate() {
    this.changeParent()
  }
  public componentDidMount() {
    this.renderPermissions()
  }
  public changeParent() {
    this.props.onChange(this.state)
  }
  public renderPermissions() {
    this.setState(this.props.userPermissions)
  }
  public render() {
    console.log(this.props.levels)
    this.levels = this.props.levels
    return (
      <div>
        <h4 style={{ marginTop: '15px' }}>Permissões gerais do usuário</h4>
        <h5>
          Essas são as permissões e/ou cargos que este usuário terá acesso
        </h5>

        <div style={this.styleChk}>
          <Row>
            <Grid cols="12 12 6 6">
              <div style={this.styleBoxCheck}>
                <input
                  id="chkDirector"
                  name="director"
                  type="checkbox"
                  onChange={e => this.handleChangeMainCheckBox(e)}
                />
                <label htmlFor="chkDirector" style={this.styleLabel}>
                  Diretor
                </label>
              </div>
            </Grid>
            <Grid cols="12 12 6 6">
              <div style={this.styleBoxCheck}>
                <input
                  id="chkBuyer"
                  name="buyer"
                  type="checkbox"
                  onChange={e => this.handleChangeMainCheckBox(e)}
                />
                <label htmlFor="chkBuyer" style={this.styleLabel}>
                  Comprador
                </label>
              </div>
            </Grid>
          </Row>
          <Row>
            <Grid cols="12 12 6 6">
              <div style={this.styleBoxCheck}>
                <input
                  id="chkKeeper"
                  name="keeper"
                  type="checkbox"
                  onChange={e => this.handleChangeMainCheckBox(e)}
                />
                <label htmlFor="chkKeeper" style={this.styleLabel}>
                  Mantenedor
                </label>
              </div>
            </Grid>
            <Grid cols="12 12 6 6">
              <div style={this.styleBoxCheck}>
                <input
                  id="chkAdmin"
                  name="admin"
                  type="checkbox"
                  onChange={e => this.handleChangeMainCheckBox(e)}
                />
                <label htmlFor="chkAdmin" style={this.styleLabel}>
                  Administrador
                </label>
              </div>
            </Grid>
          </Row>
        </div>

        <div style={this.styleChk}>
          <Row>
            <Grid cols="12 12 12 12">
              <div>
                <div style={this.styleBoxCheck}>
                  <input
                    id="chkCoordinator"
                    name="coordinator"
                    type="checkbox"
                    onChange={e => this.handleChangeMainCheckBox(e)}
                  />
                  <label htmlFor="chkCoordinator" style={this.styleLabel}>
                    Coordenador
                  </label>
                </div>
                <div style={{ clear: 'both' }} />
                {this.state.profiles.coordinator === true ? (
                  <Row> {this.renderLevels()}</Row>
                ) : (
                  ''
                )}
              </div>
            </Grid>
          </Row>
        </div>
        <div style={this.styleChk}>
          <Row>
            <Grid cols="12 12 12 12">
              <div>
                <div style={this.styleBoxCheck}>
                  <input
                    id="chkTeacher"
                    name="teacher"
                    type="checkbox"
                    onChange={e => this.handleChangeMainCheckBox(e)}
                  />
                  <label htmlFor="chkTeacher" style={this.styleLabel}>
                    Professor
                  </label>
                </div>
                <div style={{ clear: 'both' }} />
                {this.state.profiles.teacher === true ? (
                  <Row> {this.renderLevels(this.PROFILE_LEVELS.TEACHER)}</Row>
                ) : (
                  ''
                )}
              </div>
            </Grid>
          </Row>
        </div>
      </div>
    )
  }
}
