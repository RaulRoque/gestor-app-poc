import * as React from 'react';
import { CSSProperties } from 'react';

interface IProps {
  onClose?: any
  items?: any
}

class Chip extends React.Component<IProps> {

  public closeStyle: CSSProperties = {
    fontSize: '18px',
    paddingLeft: '10px',
    paddingRight: '5px',
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 1,
    letterSpacing: 'normal',
    textTransform: 'none',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    wordWrap: 'normal',
    direction: 'ltr',
    cursor: 'pointer',
  }
  public chipStyle: CSSProperties = {
    background: '#2ABB9C',
    color: '#fff',
    display: 'inline-block',
    height: '32px',
    fontSize: '13px',
    fontWeight: 500,
    lineHeight: '32px',
    padding: '0px 10px 0px 15px',
    borderRadius: '16px',
    marginBottom: '5px',
    marginRight: '5px',
    boxSizing: 'inherit',
  }

  constructor(props) {
    super(props)
    this.bindHandlers()
  }

  public bindHandlers() {
    this.handleOnClose = this.handleOnClose.bind(this)
  }

  public handleOnClose(e) {
    this.props.onClose({
      index: e.target.dataset.index,
      item: {
        value: parseInt(e.target.dataset.value),
        label: e.target.dataset.label,
      },
    })
  }

  public render() {
    return (
      <div>
        {this.props.items.map((item, i) => (
          <span style={this.chipStyle}>
            {item.label}{' '}
            <i
              style={this.closeStyle}
              key={item.value}
              data-index={i}
              data-value={item.value}
              data-label={item.label}
              onClick={this.handleOnClose}
            >
              x
            </i>
          </span>
        ))}
      </div>
    )
  }
}

export default Chip
