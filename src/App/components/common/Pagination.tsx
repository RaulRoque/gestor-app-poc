import * as React from 'react';

interface IProps {
  totalRecords?: any,
  recordsPerPage?: any,
  currentPage?: any,
  callbackView?: any,
}

class Pagination extends React.PureComponent<IProps> {
  constructor(props) {
    super(props)
    this.bindHandlers()
  }

  public bindHandlers() {
    this.printPages = this.printPages.bind(this)
    this.handleClickFilter = this.handleClickFilter.bind(this)
  }
  public handleChangeGrade(value) {
    this.setState({ selectedGrade: value })
  }

  public handleClickFilter(e) {
    this.props.callbackView(e.target.dataset.value)
  }
  public calcPages() {
    return Math.round(this.props.totalRecords / this.props.recordsPerPage)
  }

  public calcFivePagesNext(page, numPages) {
    page = page + 1
    numPages = numPages - 1
    let fiveNext = page + 4
    const childrenNext = []
    if (fiveNext >= numPages) {
      const rest = numPages - page
      fiveNext = rest + page
    }
    for (let i = page; i <= fiveNext; i++) {
      childrenNext.push(
        <li key={i} data-value={i} onClick={this.handleClickFilter}>
          {i}
        </li>,
      )
    }
    if (numPages - fiveNext >= 1) {
      childrenNext.push(<li>...</li>)
    }
    return childrenNext
  }

  public calcFivePagesPrev(page) {
    page = page - 5
    let fivePrev = page + 4
    const childrenPrev = []
    if (page <= 1) {
      fivePrev = 5 + page
      page = 2
    }
    if (page >= 3) {
      childrenPrev.push(<li>...</li>)
    }
    for (let i = page; i <= fivePrev; i++) {
      if (i != this.props.currentPage) {
        childrenPrev.push(
          <li key={i} data-value={i} onClick={this.handleClickFilter}>
            {i}
          </li>,
        )
      }
    }
    return childrenPrev
  }

  public printPages() {
    const numPages = this.calcPages()
    const children = []
    for (let i = 1; i <= numPages; i++) {
      if (i === this.props.currentPage) {
        children.push(this.calcFivePagesPrev(i))
        children.push(
          <li key={i} className="active">
            {i}
          </li>,
        )
        children.push(this.calcFivePagesNext(i, numPages))
      } else {
        if (i === 1) {
          children.push(
            <li key={i} data-value={i} onClick={this.handleClickFilter}>
              {i}
            </li>,
          )
        }
        if (i === numPages) {
          children.push(
            <li key={i} data-value={i} onClick={this.handleClickFilter}>
              {i}
            </li>,
          )
        }
      }
    }
    return children
  }

  public pagePrev() {
    const prevPage = this.props.currentPage - 1
    if (prevPage > 0) {
      this.props.callbackView(prevPage)
    }
  }

  public pageNext() {
    const nextPage = this.props.currentPage + 1
    const numPages = this.calcPages()
    if (nextPage <= numPages) {
      this.props.callbackView(nextPage)
    }
  }

  public render() {
    return (
      <React.Fragment>
        <ul className="pagination">
          <li onClick={() => this.pagePrev()}> {'< Anterior'}</li>
          {this.printPages()}
          <li onClick={() => this.pageNext()}> {'Proximo >'} </li>
        </ul>
      </React.Fragment>
    )
  }
}

export default Pagination
