import Dropdown from 'plurall-dropdown'
import * as React from 'react';
import * as URL from 'url-parse'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'

interface IState {
  mobile?: boolean,
  openHelp?: boolean
}

interface IProps {
  internalPaths: string;
  subDomain: string;
  wrapper: string;
}

class Header extends React.PureComponent<IProps, IState> {

  public static defaultProps: Partial<IProps> = {
    internalPaths: "",
    subDomain: ""
  };

  public state: IState = {
    mobile: false,
    openHelp: false
  };

  public normalizeItem = (item: any) => {
    const newItem: any = {}
    newItem.id = item.id
    newItem.name = item.name

    if (item.link_url !== 'http:nolink') {
      const urlLink = new URL(item.link_url)
      const projectUrl = new URL(item.link_url)
      const path = `${urlLink.pathname}${urlLink.query}`

      newItem.link_url =
        this.props.internalPaths.includes(path) &&
        projectUrl.hostname.includes(this.props.subDomain)
          ? path
          : item.link_url
    }

    if (item.sub_items) {
      newItem.sub_items = item.sub_items.map(this.normalizeItem)
    }

    return newItem
  }

  public toggleMobileMenu = () =>
    this.setState({
      mobile: !this.state.mobile,
    })

  public toggleHelp = (active: any) => {
    this.setState({ openHelp: !active })
  }

  public render() {
    const styles: any = {}
    if (this.props.wrapper) {
      styles.width = this.props.wrapper
    }

    return (
      <React.Fragment>
        <div style={styles} className="menu">
          <div className="logo" />
          <Row>
            <Grid cols="12 4 4 4">
              <h1>Gestor de Turmas</h1>
            </Grid>
            <Grid cols="12 5 5 5">
              <div style={{ width: '100%', padding: '7px' }}>
                <Dropdown
                  name="Ano"
                  options={[{ label: 'SUA ESCOLA', value: '123456' }]}
                />
              </div>
            </Grid>
            <Grid cols="12 3 3 3">
              <div style={{ width: '100%', padding: '7px' }}>
                <Dropdown
                  name="Ano"
                  options={[
                    { label: 'Bem vindo, usuário!', value: '' },
                    { label: 'Minha Conta', value: 'my-account' },
                    { label: 'Sair', value: 'logout' },
                  ]}
                />
              </div>
            </Grid>
          </Row>
        </div>
      </React.Fragment>
    )
  }
}

/*Header.propTypes = {

}

Header.defaultProps = {

}*/

export default Header
