import * as React from 'react';
import { CSSProperties } from 'react';

interface IProps {
  items: []
  itemsChecked: []
  rowCheckbox: boolean
  onChangeCheckBoxOne: any
  backgroundActive: string
  background: string
}

interface IState {
  checkedRows?: any
  checkAll?: any
  selectedYear?: any
}

export default class CheckboxOne extends React.PureComponent<IProps, IState> {
  public pullLeftCheck: CSSProperties = {
    float: 'left',
    margin: '2px 10px 0 0',
    width: '2%',
  }

  public checkBoxGroupText: CSSProperties = {
    width: '96%',
    float: 'left',
  }

  public checkBoxGroupTextNames: CSSProperties = {
    width: '50%',
    float: 'left',
  }

  public checkBoxGroupTextExtras: CSSProperties = {
    width: '50%',
    float: 'left',
  }

  public checkboxGroupListActive: CSSProperties = {
    margin: '0 0 5px 0',
    padding: '10px',
    background: this.props.backgroundActive,
    borderRadius: '6px',
    float: 'left',
    width: '100%',
    border: '2px solid #2ABB9C',
  }

  public checkboxGroupList: CSSProperties = {
    margin: '0 0 5px 0',
    padding: '10px',
    background: this.props.background,
    borderRadius: '6px',
    float: 'left',
    width: '100%',
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    this.initChecked()
  }

  public getInitialState() {
    return {
      checkedRows: [],
    }
  }

  public initChecked() {
    const checkedRows = this.state.checkedRows
    if (this.props.itemsChecked) {
      this.props.itemsChecked.forEach((element, index) => {
        checkedRows.push(element['value'])
      })
      this.setState({ checkedRows: checkedRows.slice() })
    }
  }

  public bindHandlers() {
    this.checkRow = this.checkRow.bind(this)
  }

  public checkRow(e) {
    const checkedRows = []
    checkedRows.push(parseInt(e.target.value))
    this.setState({ checkedRows: checkedRows.slice() })
    this.props.onChangeCheckBoxOne(e.target.value)
  }

  public renderRows(item, key) {
    const children = []
    if (this.props.rowCheckbox) {
      children.push(
        <div key={item.value} style={this.pullLeftCheck}>
          <input
            type="radio"
            id={item.value}
            name="checkboxOne"
            value={item.value}
            onClick={this.checkRow}
            checked={this.state.checkedRows.indexOf(item.value) != -1}
          />
        </div>,
      )
    }
    children.push(
      <div style={this.checkBoxGroupText}>
        <div style={this.checkBoxGroupTextNames}>{item.label}</div>
        <div style={this.checkBoxGroupTextExtras}>{item.extra}</div>
      </div>,
    )
    return children
  }

  public render() {
    return (
      <React.Fragment>
        {this.props.items.map((item, i) => (
          <div>
            <div
              key={i}
              className="dataRow"
              style={
                this.state.checkedRows.includes(item['value'])
                  ? this.checkboxGroupListActive
                  : this.checkboxGroupList
              }
            >
              {this.renderRows(item, i)}
            </div>
          </div>
        ))}
      </React.Fragment>
    )
  }
}
