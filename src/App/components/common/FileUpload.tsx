import * as React from 'react';
import { Button } from 'plurall-button'
import { Upload } from 'plurall-icons'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import { CSSProperties } from 'react';

interface IState {
  selectedFile?: any
}

export class FileUpload extends React.PureComponent<any, IState> {

  public fileStyle: CSSProperties = {
    opacity: 0,
    position: 'absolute',
    pointerEvents: 'none',
    width: '1px',
    height: '1px',
  }

  public dropzoneClickAreaStyle: CSSProperties = {
    border: '2px dashed rgb(30, 182, 153)',
    borderRadius: '5px',
    padding: '10px',
    width: '100%',
    marginTop: '10px',
  }

  public pStyle: CSSProperties = {
    display: 'block',
    fontSize: '16px',
    margin: '0',
    color: '#293A4C',
    fontWeight: 300,
  }

  constructor(props) {
    super(props)
    this.bindHandlers()
    this.state = {
      selectedFile: false,
    }
  }

  public bindHandlers() {}
  public handleChange(e) {
    console.log(e.target.files[0])
    this.setState({ selectedFile: e.target.files[0] })
  }

  public dragover(e) {
    console.log(e)
    console.log(e.dataTransfer.files[0])
    this.setState({ selectedFile: e.dataTransfer.files[0] })
    e.preventDefault()
  }
  public dragenter(e) {
    e.preventDefault()
  }
  public drop() {
    console.log('1')
  }
  public allowDrop(ev) {
    console.log(document.getElementById('dropZone'))
    document.getElementById('dropZone').style.background = 'rgb(246, 246, 246)'
    ev.preventDefault()
  }
  public ondragleave(ev) {
    document.getElementById('dropZone').style.background = '#fff'
    ev.preventDefault()
  }
  public getSelectedFileName() {
    return this.state.selectedFile
      ? this.state.selectedFile.name
      : 'Nenhum Arquivo informado.'
  }
  public render() {
    return (
      <React.Fragment>
        <div>
          <div>
            <div
              draggable={true}
              style={this.dropzoneClickAreaStyle}
              onDragOver={e => this.allowDrop(e)}
              onDragLeave={e => this.ondragleave(e)}
              onDrop={e => this.dragover(e)}
              id="dropZone"
            >
              <input
                type="file"
                id="spreadsheetSchoolStaffFile"
                style={this.fileStyle}
                onChange={e => this.handleChange(e)}
              />
              <label htmlFor="spreadsheetSchoolStaffFile">
                <Row>
                  <Grid cols="12 4 2 2">
                    <Upload size={60} color="rgb(30, 182, 153)" />
                  </Grid>
                  <Grid cols="12 8 10 10">
                    Faça o upload da planilha
                    <p style={this.pStyle}>
                      Clique ou arraste sua planilha com os dados.
                    </p>
                  </Grid>
                </Row>
              </label>
            </div>
            <p>
              Arquivo selecionado: <strong>{this.getSelectedFileName()}</strong>
            </p>
          </div>
        </div>
      </React.Fragment>
    )
  }
}
