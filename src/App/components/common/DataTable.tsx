import * as React from 'react';
import { CSSProperties } from 'react';

interface IProps {
  headers?: any,
  items?: any,
  rowCheckbox?: boolean,
  onChangeCheckBox?: any,
}

interface IState {
  checkAll: any
  checkedRows: any
}

class DataTable extends React.PureComponent<IProps, IState> {

  public tableStyle: CSSProperties = {
    margin: '50px 0 0 0',
    width: '100%',
    display: 'table',
  }
  public tableHeaderStyle: CSSProperties = {
    color: '#fff',
    padding: '10px 20px 10px 10px',
    borderRadius: 0,
    borderLeft: '1px dashed #dfdfdf',
    fontWeight: 'normal',
    textAlign: 'left',
  }
  public tableCellStyle: CSSProperties = {
    padding: '10px 20px 10px 10px',
    fontSize: '15px',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    textAlign: 'left',
    color: '#354a5d',
    borderLeft: '1px dashed #dfdfdf',
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()

    const evenRows = 'tr.dataRow:nth-child(even) {background: #f6fbfc}'
    const oddRows = 'tr.dataRow:nth-child(odd) {background: #dde4e5 !important}'
    const headLeftCell =
      'tr th:first-child {border-top-left-radius: 7px !important;}'
    const headRightCell =
      'tr th:last-child {border-top-right-radius: 7px !important;}'

    const styleSheet = document.styleSheets[0]
    styleSheet['insertRule'](evenRows, styleSheet['cssRules'].length)
    styleSheet['insertRule'](oddRows, styleSheet['cssRules'].length)
    styleSheet['insertRule'](headLeftCell, styleSheet['cssRules'].length)
    styleSheet['insertRule'](headRightCell, styleSheet['cssRules'].length)
  }

  public getInitialState() {
    return {
      checkAll: false,
      checkedRows: [],
    }
  }
  public componentDidUpdate(prevProps) {
    if (prevProps.items !== this.props.items) {
      this.setState({ checkAll: false, checkedRows: [] })
    }
    if (this.props.onChangeCheckBox) {
      this.props.onChangeCheckBox(this.state.checkedRows)
    }
  }
  public bindHandlers() {
    this.handleCheckAll = this.handleCheckAll.bind(this)
    this.checkRow = this.checkRow.bind(this)
  }
  public handleCheckAll(e) {
    let checkedRows = this.state.checkedRows
    this.props.items.forEach((element, index) => {
      if (!this.state.checkAll) {
        if (checkedRows.indexOf(index) === -1) {
          checkedRows = checkedRows.concat(index)
        }
      } else {
        checkedRows.splice(checkedRows.indexOf(index), 1)
        checkedRows = checkedRows.slice()
      }
    })
    this.setState({ checkAll: !this.state.checkAll, checkedRows: checkedRows })
  }

  public checkRow(e) {
    this.setState({ checkAll: false })
    const checkedRows = this.state.checkedRows
    const index = parseInt(e.target.name)
    const shouldCheck = this.state.checkedRows.indexOf(index) === -1
    if (shouldCheck) {
      this.setState({ checkedRows: checkedRows.concat(index) })
    } else {
      checkedRows.splice(checkedRows.indexOf(index), 1)
      this.setState({ checkedRows: checkedRows.slice() })
    }
  }

  public renderHeaders() {
    const headers = []
    if (this.props.rowCheckbox) {
      headers.push(
        <th key={-1} style={this.tableHeaderStyle}>
          <input
            type="checkbox"
            checked={this.state.checkAll}
            onChange={this.handleCheckAll}
          />
        </th>,
      )
    }
    headers.push(
      this.props.headers.map(header => (
        <th key={header} style={this.tableHeaderStyle}>
          {header}
        </th>
      )),
    )
    return headers
  }
  public renderRows(item, index) {
    const children = []
    if (this.props.rowCheckbox) {
      children.push(
        <td key={index} style={this.tableCellStyle}>
          <input
            type="checkbox"
            onChange={this.checkRow}
            checked={this.state.checkedRows.indexOf(index) != -1}
            name={index}
          />
        </td>,
      )
    }
    children.push(
      item.map(row => (
        <td key={row.toString()} style={this.tableCellStyle}>
          {row}
        </td>
      )),
    )
    return children
  }
  public render() {
    return (
      <React.Fragment>
        <table style={this.tableStyle}>
          <tbody>
            <tr style={{ background: '#354A5D' }}>{this.renderHeaders()}</tr>
            {this.props.items.map((item, i) => (
              <tr key={i} className="dataRow">
                {this.renderRows(item, i)}
              </tr>
            ))}
          </tbody>
        </table>
      </React.Fragment>
    )
  }
}

export default DataTable
