import * as React from 'react';
import Dropdown from 'plurall-dropdown'
import Grid from '../../../containers/Grid'
import Row from '../../../containers/Row'
import FilterService from '../../../services/FilterServices'
import { Button } from 'plurall-button'
import { Input } from 'plurall-form'
import { Title } from 'plurall-texts'

interface IProps {
  callbackView?: any,
}

interface IState {
  selectedProfile: any
  selectedLevel: any
  selectedGrade: any
  selectedSchoolClass: any
  selectedStatus: any
  className: any
  schoolClasses: any
  levels: any
  grades: any
}

class StudentParentFilter extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    this.getLevels()
  }

  public getInitialState() {
    return {
      selectedProfile: '',
      selectedLevel: '',
      selectedGrade: '',
      selectedSchoolClass: '',
      selectedStatus: '',
      className: '',
      schoolClasses: [{ label: 'Turma', value: '' }],
      levels: [{ label: 'Segmento', value: '' }],
      grades: [{ label: 'Série', value: '' }],
    }
  }

  public bindHandlers() {
    this.handleChangeProfile = this.handleChangeProfile.bind(this)
    this.handleChangeLevel = this.handleChangeLevel.bind(this)
    this.handleChangeGrade = this.handleChangeGrade.bind(this)
    this.handleChangeSchoolClasses = this.handleChangeSchoolClasses.bind(this)
    this.handleChangeStatus = this.handleChangeStatus.bind(this)
    this.handleChangeClassName = this.handleChangeClassName.bind(this)
    this.handleClickFilter = this.handleClickFilter.bind(this)
  }

  public handleChangeProfile(value) {
    this.setState({ selectedProfile: value })
  }

  public handleChangeLevel(value) {
    this.setState({ selectedLevel: value })
    this.getGrades(value)
  }

  public handleChangeGrade(value) {
    this.setState({ selectedGrade: value })
    this.getSchoolClasses(value)
  }

  public handleChangeSchoolClasses(value) {
    this.setState({ selectedSchoolClass: value })
  }

  public handleChangeStatus(value) {
    this.setState({ selectedStatus: value })
  }

  public handleChangeClassName(event) {
    this.setState({ className: event.target.value })
  }

  public handleClickFilter(value) {
    const filters = {
      profile: this.state.selectedProfile,
      level: this.state.selectedLevel,
      grade: this.state.selectedGrade,
      schoolClass: this.state.selectedSchoolClass,
      status: this.state.selectedStatus,
      className: this.state.className,
    }
    this.props.callbackView(filters)
  }

  // Get's
  public async getLevels() {
    const data = await FilterService.getLevels()
    this.setState({
      levels: data['levels'],
    })
  }

  public async getGrades(level) {
    if (level) {
      const data = await FilterService.getGrades(level)
      this.setState({
        grades: data['grades'],
      })
    }
  }

  public async getSchoolClasses(filters) {
    if (filters) {
      const data = await FilterService.getSchoolClasses(filters)
      this.setState({ schoolClasses: data['schoolClasses'] })
    }
  }

  public render() {
    return (
      <React.Fragment>
        <Title size="small">Filtros</Title>
        <Row>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="profile"
                options={[
                  { label: 'perfil', value: '' },
                  { label: 'Aluno', value: 'Aluno' },
                  { label: 'Responsável', value: 'Responsável' },
                ]}
                onChange={this.handleChangeProfile}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="level"
                options={this.state.levels}
                onChange={this.handleChangeLevel}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="grade"
                options={this.state.grades}
                onChange={this.handleChangeGrade}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="schoolClasses"
                options={this.state.schoolClasses}
                onChange={this.handleChangeSchoolClasses}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="status"
                options={[
                  { label: 'Status', value: '' },
                  { label: 'Ativo', value: 'ativo' },
                  { label: 'Inativo', value: 'inativo' },
                  { label: 'Pendente', value: 'pendente' },
                ]}
                onChange={this.handleChangeStatus}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Input
                type="text"
                name="className"
                placeholder="Nome"
                onChange={this.handleChangeClassName}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Button type="primary" onClick={this.handleClickFilter}>
                Filtrar
              </Button>
            </div>
          </Grid>
        </Row>
      </React.Fragment>
    )
  }
}

export default StudentParentFilter
