import * as React from 'react';
import Dropdown from 'plurall-dropdown'
import Grid from '../../../containers/Grid'
import Row from '../../../containers/Row'
import FilterService from '../../../services/FilterServices'
import { Button } from 'plurall-button'
import { Title } from 'plurall-texts'

interface IProps {
  history?: any
  callbackView?: any
}

interface IState {
  levels: any
  grades: any
  selectedYear?: any
  selectedGrade: any
  selectedOrder: any
  selectedLevel: any
  selectedWithUsers: any
}

class SchoolClassFilter extends React.PureComponent<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      selectedYear: '',
      selectedLevel: '',
      selectedGrade: '',
      selectedOrder: '',
      selectedWithUsers: '',

      levels: [{ label: 'Segmento', value: '' }],
      grades: [{ label: 'Série', value: '' }],
    }
  }
  public bindHandlers() {
    this.handleChangeYear = this.handleChangeYear.bind(this)
    this.handleChangeLevel = this.handleChangeLevel.bind(this)
    this.handleChangeGrade = this.handleChangeGrade.bind(this)
    this.handleClickFilter = this.handleClickFilter.bind(this)
  }

  public handleChangeYear(value) {
    this.setState({ selectedYear: value })
    this.getFilters(value, null)
  }
  public handleChangeLevel(value) {
    this.setState({ selectedLevel: value })
    this.getFilters(null, value)
  }
  public handleChangeGrade(value) {
    this.setState({ selectedGrade: value })
  }

  public handleClickFilter(value) {
    const filters = {
      year: this.state.selectedYear,
      grade: this.state.selectedGrade,
      order: this.state.selectedOrder,
      withUsers: this.state.selectedWithUsers,
    }
    this.props.callbackView(filters)
  }

  public getYears() {
    return [
      { label: 'Ano Letivo', value: '' },
      { label: '2018', value: '2018' },
      { label: '2017', value: '2017' },
      { label: '2016', value: '2016' },
    ]
  }

  public async getFilters(year, level) {
    if (year) {
      const data = await FilterService.getLevels(year)
      this.setState({ levels: data['levels'] })
    }
    if (level) {
      const data = await FilterService.getGrades(level)
      this.setState({ grades: data['grades'] })
    }
  }
  public render() {
    return (
      <React.Fragment>
        <Title size="small">Filtros</Title>
        <Row>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="year"
                options={this.getYears()}
                onChange={this.handleChangeYear}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="levels"
                options={this.state.levels}
                onChange={this.handleChangeLevel}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="grades"
                options={this.state.grades}
                onChange={this.handleChangeGrade}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="withUsers"
                options={[
                  { label: 'Com Usuário', value: '' },
                  { label: 'Sim', value: 'sim' },
                  { label: 'Não', value: 'nao' },
                ]}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="order"
                options={[
                  { label: 'Ordenação', value: '' },
                  { label: 'Ordem Alfabética', value: 'alfabetica' },
                  { label: 'Recentemente Criadas', value: 'criacao' },
                ]}
              />
            </div>
          </Grid>
          <Grid cols="12 6 2 2">
            <div style={{ width: '100%' }}>
              <Button
                id="btnid123"
                type="primary"
                onClick={this.handleClickFilter}
              >
                Filtrar
              </Button>
            </div>
          </Grid>
        </Row>
      </React.Fragment>
    )
  }
}

export default SchoolClassFilter
