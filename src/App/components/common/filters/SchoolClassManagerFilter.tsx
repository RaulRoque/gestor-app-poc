import * as React from 'react';
import Dropdown from 'plurall-dropdown'
import Grid from '../../../containers/Grid'
import Row from '../../../containers/Row'
import { Button } from 'plurall-button'
import { Input } from 'plurall-form'
import { Title } from 'plurall-texts'

interface IProps {
  callbackView?: any,
  onFilter?: any
}

interface IState {
  selectedProfile?: any,
  selectedName?: any
}

class SchoolClassFilter extends React.PureComponent<IProps, IState> {
  public handleChangeYear: any

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      selectedProfile: '',
      selectedName: '',
    }
  }
  public bindHandlers() {
    this.handleClickFilter = this.handleClickFilter.bind(this)
    this.handleChangeText = this.handleChangeText.bind(this)
  }

  public handleClickFilter(value) {
    const filters = {
      profile: this.state.selectedProfile,
      name: this.state.selectedName,
    }
    this.props.onFilter(filters)
  }
  public handleChangeText(event) {
    this.setState({ selectedName: event.target.value })
  }

  public getProfiles() {
    return [
      { label: 'Perfil', value: '' },
      { label: 'Alunos e Responsáveis', value: '1' },
      { label: 'Equipe Escolar', value: '2' },
    ]
  }

  public render() {
    return (
      <React.Fragment>
        <Title size="small">Filtros</Title>
        <Row>
          <Grid cols="12 6 4 4">
            <div style={{ width: '100%' }}>
              <Dropdown
                name="year"
                options={this.getProfiles()}
                onChange={this.handleChangeYear}
              />
            </div>
          </Grid>
          <Grid cols="12 6 4 4">
            <div style={{ width: '100%' }}>
              <Input
                type="text"
                placeholder="Buscar"
                onChange={this.handleChangeText}
                value={this.state.selectedName}
              />
            </div>
          </Grid>
          <Grid cols="12 6 4 4">
            <div style={{ width: '100%' }}>
              <Button type="primary" onClick={this.handleClickFilter}>
                Filtrar
              </Button>
            </div>
          </Grid>
        </Row>
      </React.Fragment>
    )
  }
}

export default SchoolClassFilter
