import * as React from 'react';
import { CSSProperties } from 'react';

interface IProps {
  items: []
  itemsChecked: []
  rowCheckbox: boolean
  onChangeCheckListOne: any
  backgroundActive: string
  background: string
}

interface IState {
  checkedRows?: any
  checkAll?: any
  selectedYear?: any
  checkedLists?: any
}

export default class CheckListOne extends React.PureComponent<IProps, IState> {
  public checkBoxGroupText: CSSProperties = {
    width: '96%',
    float: 'left',
  }

  public checkboxGroupListActive: CSSProperties = {
    position: 'relative',
    margin: '0 0 5px 0',
    padding: '10px',
    background: this.props.backgroundActive,
    borderRadius: '5px',
    float: 'left',
    width: '100%',
    border: '5px solid #2ABB9C',
    cursor: 'pointer',
    height: '100%',
  }

  public checkboxGroupList: CSSProperties = {
    position: 'relative',
    margin: '0 0 5px 0',
    padding: '10px',
    background: this.props.background,
    borderRadius: '6px',
    float: 'left',
    width: '100%',
    height: '100%',
  }

  public ul: CSSProperties = {
    margin: '15px -24px',
  }

  public list: CSSProperties = {
    margin: '10px',
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    this.initCheckedList()
  }

  public getInitialState() {
    return {
      checkedLists: [],
    }
  }

  public initCheckedList() {
    const checkedLists = this.state.checkedLists
    if (this.props.itemsChecked) {
      this.props.itemsChecked.forEach(e => {
        checkedLists.push(e['value'])
      })
      this.setState({ checkedLists: checkedLists.slice() })
    }
  }

  public bindHandlers() {
    this.checkedList = this.checkedList.bind(this)
  }

  public checkedList(e) {
    const checkedLists = []
    checkedLists.push(parseInt(e.target.dataset.valor))
    this.setState({ checkedLists: checkedLists.slice() })
    this.props.onChangeCheckListOne(e.target.dataset.valor)
  }

  public renderLists(lists) {
    const children = []
    lists.map(item => children.push(<li style={this.list}>{item}</li>))
    return children
  }

  public renderRows(i) {
    const children = []
    children.push(
      <div>
        <div
          data-valor={i.value}
          onClick={this.checkedList}
          style={{
            position: 'absolute',
            height: '100%',
            width: '100%',
            cursor: 'pointer',
            top: 0,
            left: 0,
          }}
        />
        <div style={this.checkBoxGroupText}>
          <h5>{i.title}</h5>
          <label>{i.subTitle}</label>
          <ul style={this.ul}>{this.renderLists(i.lists)}</ul>
          <p style={{ color: '#2ABB9C' }}>{i.extra}</p>
        </div>
      </div>,
    )
    return children
  }

  public render() {
    return (
      <React.Fragment>
        {this.props.items.map(i => (
          <div>
            <div
              className="dataRow"
              style={
                this.state.checkedLists.includes(i['value'])
                  ? this.checkboxGroupListActive
                  : this.checkboxGroupList
              }
            >
              {this.renderRows(i)}
            </div>
          </div>
        ))}
      </React.Fragment>
    )
  }
}
