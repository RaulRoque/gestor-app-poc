import * as React from 'react';
import { CSSProperties } from 'react';

interface IProps {
  items: any
  itemsChecked: any
  rowCheckbox: boolean
  onChangeCheckBox: any
  headers: any
}

interface IState {
  checkedRows?: any
  checkAll?: any
  selectedYear?: any
}

export default class CheckboxGroup extends React.Component<IProps, IState> {

  public checkboxGroupHeader: CSSProperties = {
    margin: '0 0 5px 0',
    padding: '10px',
    background: '#F1F1F1',
    borderRadius: '6px',
    width: '100%',
    float: 'left',
  }

  public pullLeftCheck: CSSProperties = {
    float: 'left',
    margin: '2px 10px 0 0',
    width: '2%',
  }

  public checkBoxGroupText: CSSProperties = {
    width: '96%',
    float: 'left',
  }

  public checkBoxGroupTextNames: CSSProperties = {
    width: '50%',
    float: 'left',
  }

  public checkBoxGroupTextExtras: CSSProperties = {
    width: '50%',
    float: 'left',
  }

  public checkboxGroupListActive: CSSProperties = {
    margin: '0 0 5px 0',
    padding: '10px',
    background: '#F1F1F1',
    borderRadius: '6px',
    float: 'left',
    width: '100%',
    border: '2px solid #2ABB9C',
  }

  public checkboxGroupList: CSSProperties = {
    margin: '0 0 5px 0',
    padding: '10px',
    background: '#F1F1F1',
    borderRadius: '6px',
    float: 'left',
    width: '100%',
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()

    const evenRows = 'tr.dataRow:nth-child(even) {background: #f6fbfc}'
    const oddRows = 'tr.dataRow:nth-child(odd) {background: #dde4e5 !important}'
    const headLeftCell =
      'tr th:first-child {border-top-left-radius: 7px !important;}'
    const headRightCell =
      'tr th:last-child {border-top-right-radius: 7px !important;}'

    const styleSheet = document.styleSheets[0]
    styleSheet['insertRule'](evenRows, styleSheet['cssRules'].length)
    styleSheet['insertRule'](oddRows, styleSheet['cssRules'].length)
    styleSheet['insertRule'](headLeftCell, styleSheet['cssRules'].length)
    styleSheet['insertRule'](headRightCell, styleSheet['cssRules'].length)
    this.initCheckedBoxGroup()
  }

  public getInitialState() {
    return {
      checkAll: false,
      checkedRows: [],
    }
  }

  public componentWillReceiveProps() {
    this.updateCheckedBoxGroup()
  }

  public initCheckedBoxGroup() {
    const checkedRows = this.state.checkedRows
    this.props.itemsChecked.forEach((element, index) => {
      checkedRows.push(element.value)
    })
    this.setState({ checkedRows: checkedRows.slice() })
    this.checkedAll()
  }

  public updateCheckedBoxGroup() {
    const checkedRows = []
    this.props.itemsChecked.forEach(element => {
      checkedRows.push(element.value)
    })
    this.setState({ checkedRows: checkedRows.slice() })
    this.checkedAll()
  }

  public checkedAll() {
    if (this.props.itemsChecked.length === this.props.items.length) {
      this.setState({ checkAll: true })
    } else {
      this.setState({ checkAll: false })
    }
  }

  public bindHandlers() {
    this.handleCheckAll = this.handleCheckAll.bind(this)
    this.checkRow = this.checkRow.bind(this)
  }

  public handleCheckAll(e) {
    let checkedRows = this.state.checkedRows
    this.props.items.forEach(element => {
      if (!this.state.checkAll) {
        if (checkedRows.indexOf(element.value) === -1) {
          this.props.onChangeCheckBox(element.value)
          checkedRows = checkedRows.concat(element.value)
        }
      } else {
        this.props.onChangeCheckBox(element.value)
        checkedRows.splice(checkedRows.indexOf(element.value), 1)
        checkedRows = checkedRows.slice()
      }
    })
    this.setState({ checkAll: !this.state.checkAll, checkedRows: checkedRows })
  }

  public checkRow(e) {
    this.setState({ checkAll: false })
    const checkedRows = this.state.checkedRows
    const index = parseInt(e.target.name, undefined)
    const shouldCheck = this.state.checkedRows.indexOf(index) === -1
    if (shouldCheck) {
      this.setState({ checkedRows: checkedRows.concat(index) })
    } else {
      checkedRows.splice(checkedRows.indexOf(index), 1)
      this.setState({ checkedRows: checkedRows.slice() })
    }
    this.props.onChangeCheckBox(e.target.value)
  }

  public renderHeaders() {
    const headers = []
    if (this.props.rowCheckbox) {
      headers.push(
        <div key={-1} style={this.pullLeftCheck}>
          <input
            type="checkbox"
            checked={this.state.checkAll}
            onChange={this.handleCheckAll}
          />
        </div>,
      )
    }
    headers.push(
      this.props.headers.map(header => <div key={header}>{header}</div>),
    )
    return headers
  }

  public renderRows(item, key) {
    const children = []
    if (this.props.rowCheckbox) {
      children.push(
        <div key={item.value} style={this.pullLeftCheck}>
          <input
            type="checkbox"
            onChange={this.checkRow}
            checked={this.state.checkedRows.indexOf(item.value) != -1}
            name={item.value}
            value={item.value}
          />
        </div>,
      )
    }
    children.push(
      <div style={this.checkBoxGroupText}>
        <div style={this.checkBoxGroupTextNames}>{item.label}</div>
        <div style={this.checkBoxGroupTextExtras}>{item.extra}</div>
      </div>,
    )
    return children
  }

  public render() {
    return (
      <React.Fragment>
        <div style={this.checkboxGroupHeader}>{this.renderHeaders()}</div>
        {this.props.items.map((item, i) => (
          <div>
            <div
              key={i}
              className="dataRow"
              style={
                this.state.checkedRows.includes(item.value)
                  ? this.checkboxGroupListActive
                  : this.checkboxGroupList
              }
            >
              {this.renderRows(item, i)}
            </div>
          </div>
        ))}
      </React.Fragment>
    )
  }
}
