import * as React from 'react';
import { Button } from 'plurall-button'
import { Title, Text } from 'plurall-texts'
import { CSSProperties } from 'react';

interface IProps {
  show?: boolean
  onClose: any
  title?: string
  labelCancelButton?: string
  children?: any
  width?: any
  height?: any
  callbackView?: any
  subTitle?: string
  labelSaveButton?: string
  disableSaveButton?: any
  extras?: string
}

class Modal extends React.Component<IProps> {

  public static defaultProps: Partial<IProps> = {
    width: 500,
    height: 300,
  };

  public ButtonEnum = {
    SAVE_BUTTON: 1,
  }

  constructor(props) {
    super(props)
    Object.freeze(this.ButtonEnum)
  }

  public render() {
    if (!this.props.show) {
      return null
    }

    const keyframesBackground = `@-webkit-keyframes showingModalBackGround {
      from {background-color: rgba(0,0,0,0)}
      to {background-color: rgba(0,0,0,0.3)}
    }`
    const keyframes = `@-webkit-keyframes showingModal {
      from {background-color: rgba(0,0,0,0)}
      to {background-color: #fff }
    }
    `

    const styleSheet = document.styleSheets[0]
    styleSheet['insertRule'](keyframes, styleSheet['cssRules'].length)
    styleSheet['insertRule'](keyframesBackground, styleSheet['cssRules'].length)

    const backdropStyle: CSSProperties = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: '5% 15%',
      animationName: 'showingModalBackGround 0.2s',
      float: 'left',
    }

    const modalStyle: CSSProperties = {
      position: 'relative',
      backgroundColor: '#FFF',
      borderRadius: '5px',
      width: '1000px',
      minHeight: '300px',
      margin: '0px auto',
      padding: '30px',
      animationName: 'showingModal',
      animationDuration: '0.2s',
      boxShadow: 'rgba(0, 0, 0, 0.3) 5px 5px 5px',
      float: 'left',
      overflow: 'auto',
      maxHeight: '90%',
      height: 'auto',
    }

    const headerTop: CSSProperties = {
      width: '100%',
      clear: 'both',
    }

    const headerTopLeft: CSSProperties = {
      float: 'left',
      width: '90%',
    }

    const headerTopRight: CSSProperties = {
      float: 'right',
      width: '10%',
      textAlign: 'right',
    }

    const classBorder: CSSProperties = {
      border: '1px solid #E7F1F2',
      width: '100%',
      clear: 'both',
      margin: '10px 0px 10px 0px',
    }

    const classBody: CSSProperties = {
      width: '100%',
      clear: 'both',
    }

    const modalFooter: CSSProperties = {
      position: 'sticky',
      bottom: '-30px',
      backgroundColor: '#fff',
      width: '100%',
      textAlign: 'right',
      height: '60px',
      padding: '10px',
      left: 0,
    }

    const classLink: CSSProperties = {
      cursor: 'pointer',
    }

    const clear: CSSProperties = {
      clear: 'both',
    }

    return (
      <div>
        <div className="backdrop" style={backdropStyle}>
          <div className="" style={modalStyle}>
            <div style={headerTop}>
              <div style={headerTopLeft}>
                <div>
                  <Title size="small" color="#293A4C">
                    {this.props.title}
                  </Title>
                </div>
                <div>
                  <Text>{this.props.subTitle}</Text>
                </div>
              </div>
              <div style={headerTopRight}>
                <div>
                  <label onClick={this.props.onClose} style={classLink}>
                    X
                  </label>
                </div>
                <div>
                  <Text>{this.props.extras || ''}</Text>
                </div>
              </div>
              <div style={clear} />
            </div>

            <hr style={classBorder} />

            <div style={classBody}>{this.props.children}</div>

            <div className="footer" style={modalFooter}>
              <Button
                background="#da544f"
                color="#FFF"
                type="secondary"
                onClick={this.props.onClose}
              >
                {this.props.labelCancelButton || <span>Cancelar</span>}
              </Button>
              {this.props.disableSaveButton === true ? (
                <Button disabled={true}>
                  {this.props.labelSaveButton || <span>Salvar</span>}
                </Button>
              ) : (
                <Button
                  type="primary"
                  background="#2ABB9C"
                  color="#FFF"
                  onClick={() =>
                    this.props.callbackView(this.ButtonEnum.SAVE_BUTTON)
                  }
                >
                  {this.props.labelSaveButton || <span>Salvar</span>}
                </Button>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Modal
