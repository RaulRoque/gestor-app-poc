import * as React from 'react';

interface IProps {
  padding?: string;
  children?: any;
}

const Wrapper: React.SFC<IProps> = (props: IProps) => (
  <div
    style={{
      margin: 'auto',
      maxWidth: '1210px',
      minWidth: '280px',
      padding: props.padding,
    }}
  >
    {props.children}
  </div>
)

Wrapper.defaultProps = {
  padding: '20px',
};

export default Wrapper
