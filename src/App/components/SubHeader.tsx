import { Link } from 'react-router-dom'
import * as React from 'react';

import { Arrow } from 'plurall-icons'
import { Button } from 'plurall-button'
import Tabs from 'plurall-tabs'
import PlurallSubHeader from 'plurall-sub-header'

import Wrapper from "src/App/components/Wrapper";

const NewMessageButton = () => (
  <Link className="new-message-link" to="/mensagens/nova">
    <Button>Nova Mensagem</Button>
  </Link>
)

const BackButton: React.SFC<IBackButtonProps> = (props: IBackButtonProps) => (
  <Link to={props.backTo}>
    <Arrow size="50" />
  </Link>
)

interface IBackButtonProps {
  backTo: string
}

const SubHeader: React.SFC<ISubHeaderProps> = (props: ISubHeaderProps) => (
  <div style={{ backgroundColor: '#fff', height: '80px' }}>
    <Wrapper>
      <PlurallSubHeader
        icon={props.backTo && <BackButton backTo={props.backTo} />}
        right={props.button && <NewMessageButton />}
      >
        <Tabs
          links={[
            { label: 'Turmas', location: '/turmas' },
            {
              label: 'Alunos e Responsávels',
              location: '/alunos-e-responsaveis',
            },
            { label: 'Equipe Escolar', location: '/equipe-escolar' },
          ]}
        />
      </PlurallSubHeader>
    </Wrapper>
  </div>
)

interface ISubHeaderProps {
  backTo?: string,
  button?: boolean,
  title: string
}

export default SubHeader
