import { ValidationRules } from "./ValudationRules";

export class XlsxParser {
  public parse(file, config = null) {
    const XLSX = require('xlsx')
    try {
      const json = this.toJson(XLSX.read(file), XLSX)
      // console.log(json)
      const headers = this.extractHeaders(json)
      if (!config) {
        return json
      }
      const rules = this.parseHeaders(headers, config)
      const jsonParsedRules = this.parseRules(json, rules)
      return jsonParsedRules
    } catch (e) {
      // console.log(e)
      throw new Error(e)
    }
  }
  public parseRules(json, rules) {
    // console.log(rules);
    // remove os headers e mantem só o os dados
    json.shift()
    const validationRules = new ValidationRules()
    const errors = []
    json.forEach(row => {
      let line = 90
      row.forEach((element, index) => {
        // console.log(index + ' - ' +element);
        line++
        const conf = rules.find(r1 => r1.index === index)
        if (conf) {
          conf.config.rules.forEach(r2 => {
            const valResult = validationRules[r2](
              element,
              conf.config.label,
              line,
            )
            // console.log(valResult);
            if (valResult.error !== undefined) {
              errors.push(valResult.error)
            }
          })
        }
      })
    })
    json['errors'] = errors
    return json
  }
  public extractHeaders(jsonNotEmpty) {
    const headerJson = jsonNotEmpty[0]
    const headers = headerJson.map(element => {
      return this.toKebabCase(element)
    })
    return headers
  }
  public parseHeaders(headers, config) {
    const ret = config.map(element => {
      const indexHeader = headers.indexOf(element.label)
      if (indexHeader !== -1) {
        return { config: element, index: indexHeader }
      } else {
        throw new Error('Oooops!')
      }
    })
    return ret
  }
  public toJson(workbook, XLSX) {
    const result = {}
    workbook.SheetNames.forEach((sheetName, index) => {
      const roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
        header: 1,
        blankrows: false,
      })
      if (roa.length) {
        result[index] = roa
        if (result[index][0].length === 0) {
          throw new Error('Oooops!')
        }
      }
    })

    return JSON.parse(JSON.stringify(result[0]))
  }

  public toKebabCase(string) {
    return string
      .replace(/[\*\(\)\/]/g, ' ')
      .trim()
      .replace(/\s+/g, '-')
      .toLowerCase()
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
  }
}
