import { XlsxParser } from './XlsxParser.js'
import { SCHOOL_CLASS_CONFIG } from './config.js'

describe('Testing functions in general xlsx files', () => {
  test('File read is NOT xls or xlsx and a error is thrown', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/index.html')
    expect(() => {
      parser.parse(file)
    }).toThrow()

    return undefined;
  })
  /*checar */
  test('File read is  xls or xlsx and parse runs integrally', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const parsed = parser.parse(file)
    expect(parsed).toBeTruthy()

    return undefined;
  })

  test('Return parser must be a json', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const parsedReturn = parser.parse(file)
    expect(typeof parsedReturn === 'object').toBeTruthy()

    return undefined;
  })

  test('First Sheet is required', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const parsedReturn = parser.parse(file)
    expect(Object.keys(parsedReturn)[0]).toBeDefined()

    return undefined;
  })

  test('First Row is required', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const parsedReturn = parser.parse(file)
    expect(parsedReturn[Object.keys(parsedReturn)[0]][0]).toBeDefined()

    return undefined;
  })

  test('First Row empty throw an error', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/headers-empty.xlsx')
    expect(() => parser.parse(file)).toThrow(Error)

    return undefined;
  })

  test('should remove empty lines', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const result = parser.parse(file)
    const allLinesFilled = result.every(element => {
      return element.length !== 0
    })
    expect(allLinesFilled).toBeTruthy()

    return undefined;
  })

  test('should thorow an error with config var', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/school-class-wrong-header.xlsx')
    expect(() => {
      parser.parse(file, SCHOOL_CLASS_CONFIG)
    }).toThrow()

    return undefined;
  })
})

describe('Testing functions with configuration files', () => {
  test('should parse headers with config var', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    const allLinesFilled = result.every(element => {
      return element.length !== 0
    })
    expect(allLinesFilled).toBeTruthy()

    return undefined;
  })

  test('should parse lines with no rules', () => {
    const TEST_CONFIG = [
      {
        label: 'nome',
        rules: [],
      },
    ]
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/lines-no-rules.xlsx')
    const result = parser.parse(file, TEST_CONFIG)
    expect(result).toBeTruthy()

    return undefined;
  })

  test('should parse xlsx with no error', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/turma1007.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    console.log(result.errors)
    expect(result['errors'].length).toBeFalsy()

    return undefined;
  })
})

describe('Testing functions with rules', () => {
  test('should throw error when required field is empty', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/required-field-empty.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    expect(result['errors'].length).toBeGreaterThan(0)
    expect(result['errors'][0].rule).toBe('required')

    return undefined;
  })

  test('should not parse headers with invalid email field', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/invalid-email-field.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    expect(result['errors'].length).toBeGreaterThan(0)
    expect(result['errors'][0].rule).toBe('email')

    return undefined;
  })

  test('should not parse headers with invalid date field format', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/invalid-birth-date-field.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    expect(result['errors'].length).toBeGreaterThan(0)
    expect(result['errors'][0].rule).toBe('date')

    return undefined;
  })

  test('should not parse headers with invalid date field', () => {
    const parser = new XlsxParser()
    const fs = require('fs')
    const file = fs.readFileSync('./test_files/invalid-date-field-2.xlsx')
    const result = parser.parse(file, SCHOOL_CLASS_CONFIG)
    expect(result['errors'].length).toBeGreaterThan(0)
    expect(result['errors'][0].rule).toBe('date')

    return undefined;
  })
})
