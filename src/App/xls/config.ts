export const SCHOOL_CLASS_CONFIG = [
  {
    label: 'nome-completo-do-aluno',
    rules: ['required', 'alphanumeric', 'nonPrintable'],
  },
  {
    label: 'e-mail-do-aluno',
    rules: ['email', 'required'],
  },
  {
    label: 'data-de-nascimento-dd-mm-aaaa',
    rules: ['date'],
  },
  {
    label: 'matricula-ra',
    rules: ['alphanumeric'],
  },
  {
    label: 'nome-completo-do-responsavel',
    rules: ['alphanumeric'],
  },
  {
    label: 'e-mail-do-responsavel',
    rules: ['alphanumeric', 'email'],
  },
  {
    label: 'nome-completo-do-responsavel-2',
    rules: ['alphanumeric'],
  },
  {
    label: 'e-mail-do-responsavel-2',
    rules: ['alphanumeric', 'email'],
  },
]

export const SCHOOL_STAFF_CONFIG = {}
