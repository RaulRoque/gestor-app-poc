export class ValidationRules {
  public alphanumeric(el) {
    // console.log("alpha: " + el);
    return {}
  }

  public email(el, fieldName, line) {
    // console.log("email: " + el);
    if (el === null) {
      return true
    }
    const er = new RegExp(
      /^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/,
    )
    if (er.test(el)) {
      return true
    }
    return {
      error: {
        line: line,
        field: fieldName,
        rule: 'email',
        msg: 'Email inválido',
      },
    }
  }
  public date(el, fieldName, line) {
    if (el === null) {
      return true
    }
    const er = /^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2})$/
    if (er.test(el)) {
      const comp = el.split('/')
      const d = parseInt(comp[0], 10)
      const m = parseInt(comp[1], 10)
      const y = parseInt(comp[2], 10)
      const y2d = parseInt(y.toString().substr(-2))
      const date = new Date(y, m - 1, d)
      if (
        (date.getFullYear() === y || y2d === y) &&
        date.getMonth() + 1 === m &&
        date.getDate() === d
      ) {
        return true
      } else {
        return {
          error: {
            line: line,
            field: fieldName,
            rule: 'date',
            msg: 'Data Inválida',
          },
        }
      }
    } else {
      return {
        error: {
          line: line,
          field: fieldName,
          rule: 'date',
          msg: 'Formato inválido',
        },
      }
    }
  }

  public required(el, fieldName, line) {
    if (el !== null) {
      return true
    } else {
      return {
        error: {
          line: line,
          field: fieldName,
          rule: 'required',
          msg: 'Campo Obrigatório',
        },
      }
    }
  }
  public nonPrintable(el) {
    // console.log("nonPrintable: " + el);
    return {}
  }
}
