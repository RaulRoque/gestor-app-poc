import gql from 'graphql-tag';

const schoolQuery = gql`
  query schoolQuery {
    school(id: 3644977) {
      legalName
      cnpj
      id
      persons(limit: 3, offset: 1) {
        id
      }
      classes {
        id
        name
        vouchers {
          classId
          voucherCode
          voucherStatusId
          voucherStatus {
            id
            name
          }
        }
      }
    }
  }
`;