/* tslint:disable */
import { GraphQLResolveInfo } from "graphql";

export type Resolver<Result, Parent = any, Context = any, Args = any> = (
  parent: Parent,
  args: Args,
  context: Context,
  info: GraphQLResolveInfo
) => Promise<Result> | Result;

export type SubscriptionResolver<
  Result,
  Parent = any,
  Context = any,
  Args = any
> = {
  subscribe<R = Result, P = Parent>(
    parent: P,
    args: Args,
    context: Context,
    info: GraphQLResolveInfo
  ): AsyncIterator<R | Result>;
  resolve?<R = Result, P = Parent>(
    parent: P,
    args: Args,
    context: Context,
    info: GraphQLResolveInfo
  ): R | Result | Promise<R | Result>;
};

/** The `Naive DateTime` scalar type represents a naive date and time withouttimezone. The DateTime appears in a JSON response as an ISO8601 formattedstring. */
export type NaiveDateTime = any;

/** The `Date` scalar type represents a date. The Date appears in a JSONresponse as an ISO8601 formatted string. */
export type Date = any;

export interface RootQueryType {
  class?: Class | null /** Get classes */;
  level?: (Level | null)[] | null /** Get level grades */;
  period?: (Period | null)[] | null /** Get periods */;
  person?: Person | null /** Get first person */;
  role?: (Role | null)[] | null /** Get roles */;
  school?: School | null /** Get school */;
}

export interface Class {
  applicationId?: number | null;
  awsId?: string | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levelGrades?: (ClassLevelGrade | null)[] | null;
  materials?: (ClassCollectionLevelGrade | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
  namedVoucherUploaded?: boolean | null;
  period?: Period | null;
  periodId?: number | null;
  positions?: (PositionClass | null)[] | null;
  products?: (ClassProduct | null)[] | null;
  school?: School | null;
  schoolId?: number | null;
  status?: string | null;
  vouchers?: (VoucherClass | null)[] | null;
}

export interface ClassLevelGrade {
  classId?: number | null;
  createdAt?: NaiveDateTime | null;
  gradeId?: number | null;
  gradeName?: string | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: string | null;
  modifiedAt?: NaiveDateTime | null;
}

export interface ClassCollectionLevelGrade {
  class?: Class | null;
  classId?: number | null;
  collection?: CollectionAdoption | null;
  collectionAdoptionId?: number | null;
  collectionAdoptionLevelGradeId?: number | null;
  createdAt?: NaiveDateTime | null;
  disciplines?: (CollectionAdoptionLevelGradeDiscipline | null)[] | null;
  gradeId?: number | null;
  gradeName?: string | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: string | null;
  modifiedAt?: NaiveDateTime | null;
}

export interface CollectionAdoption {
  adoption?: Adoption | null;
  adoptionId?: number | null;
  adoptionStatus?: AdoptionStatus | null;
  adoptionStatusId?: number | null;
  brand?: Brand | null;
  brandId?: number | null;
  collection?: Collection | null;
  collectionId?: number | null;
  collectionName?: string | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levelGrades?: (CollectionAdoptionLevelGrade | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  ownerId?: number | null;
}

export interface Adoption {
  adoptionStatus?: AdoptionStatus | null;
  adoptionStatusId?: number | null;
  collections?: (CollectionAdoption | null)[] | null;
  id?: string | null;
  ownerId?: number | null;
  products?: (ProductAdoption | null)[] | null;
  schoolId?: number | null;
  services?: (ServiceAdoption | null)[] | null;
  year?: number | null;
}

export interface AdoptionStatus {
  id?: string | null;
  name?: string | null;
}

export interface ProductAdoption {
  adoption?: Adoption | null;
  adoptionId?: number | null;
  adoptionStatus?: AdoptionStatus | null;
  adoptionStatusId?: number | null;
  companyId?: number | null;
  createdAt?: NaiveDateTime | null;
  grades?: (ProductGrade | null)[] | null;
  id?: string | null;
  isbn?: number | null;
  levels?: (ProductLevel | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  ownerId?: number | null;
  productId?: number | null;
  productTitle?: number | null;
  sku?: string | null;
}

export interface ProductGrade {
  createdAt?: NaiveDateTime | null;
  gradeId?: number | null;
  gradeName?: string | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  productId?: string | null;
}

export interface ProductLevel {
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: string | null;
  modifiedAt?: NaiveDateTime | null;
  productId?: string | null;
}

export interface ServiceAdoption {
  adoption?: Adoption | null;
  adoptionId?: number | null;
  adoptionStatus?: AdoptionStatus | null;
  adoptionStatusId?: number | null;
  application?: Application | null;
  applicationId?: number | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levelGrades?: (CollectionAdoptionLevelGrade | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  ownerId?: number | null;
  service?: Service | null;
  serviceId?: number | null;
  serviceName?: string | null;
  url?: string | null;
}

export interface Application {
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
  url?: string | null;
}

export interface CollectionAdoptionLevelGrade {
  collectionAdoption?: CollectionAdoption | null;
  collectionAdoptionId?: number | null;
  createdAt?: NaiveDateTime | null;
  disciplines?: (CollectionAdoptionLevelGradeDiscipline | null)[] | null;
  gradeId?: number | null;
  gradeName?: string | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: string | null;
  modifiedAt?: NaiveDateTime | null;
}

export interface CollectionAdoptionLevelGradeDiscipline {
  collectionAdoptionLevelGrade?: CollectionAdoptionLevelGrade | null;
  collectionAdoptionLevelGradeId?: number | null;
  createdAt?: NaiveDateTime | null;
  curricularComponentId?: number | null;
  curricularComponentName?: string | null;
  disciplineId?: number | null;
  disciplineName?: string | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
}

export interface Service {
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  idApplication?: number | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
  url?: string | null;
}

export interface Brand {
  id?: string | null;
  name?: string | null;
}

export interface Collection {
  codeCollection?: number | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
}

export interface Period {
  createdAt?: NaiveDateTime | null;
  endAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  periodStatusId?: number | null;
  startAt?: NaiveDateTime | null;
  year?: number | null;
}

export interface PositionClass {
  class?: Class | null;
  classId?: number | null;
  createdAt?: NaiveDateTime | null;
  endAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  position?: Position | null;
  positionClassStatusId?: number | null;
  positionId?: number | null;
  startAt?: NaiveDateTime | null;
  status?: PositionClassStatus | null;
}

export interface Position {
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levels?: (PositionLevel | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  person?: Person | null;
  personId?: number | null;
  positionClasses?: (PositionClass | null)[] | null;
  positionStatus?: PositionStatus | null;
  positionStatusId?: number | null;
  role?: Role | null;
  roleId?: number | null;
  school?: School | null;
  schoolId?: number | null;
}

export interface PositionLevel {
  createdAt?: NaiveDateTime | null;
  disciplines?: (PositionLevelDiscipline | null)[] | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: string | null;
  modifiedAt?: NaiveDateTime | null;
  positionId?: string | null;
}

export interface PositionLevelDiscipline {
  createdAt?: NaiveDateTime | null;
  disciplineId?: number | null;
  disciplineName?: string | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  positionLevelId?: string | null;
}

export interface Person {
  activationCode?: string | null;
  activationDate?: NaiveDateTime | null;
  birthDate?: Date | null;
  cpf?: string | null;
  createdAt?: NaiveDateTime | null;
  email?: string | null;
  enrollment?: string | null;
  gender?: string | null;
  id?: string | null;
  inactivationDate?: NaiveDateTime | null;
  isResponsableCpf?: number | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
  optionInEmail?: string | null;
  optionInSms?: string | null;
  optionOfUse?: string | null;
  parentId?: number | null;
  pessoa?: Pessoa | null;
  positions?: (Position | null)[] | null;
  relatedPersons?: (RelatedPerson | null)[] | null;
  rg?: string | null;
  role?: Role | null;
  usedVouchers?: (UsedVoucher | null)[] | null;
}

export interface Pessoa {
  id?: string | null;
  name?: string | null;
  person?: Person | null;
  school?: School | null;
  syncAt?: NaiveDateTime | null;
}

export interface School {
  adoptions?: (Adoption | null)[] | null;
  classes?: (Class | null)[] | null;
  cnpj?: string | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  legalName?: string | null;
  levels?: (SchoolLevel | null)[] | null;
  mecCode?: string | null;
  modifiedAt?: NaiveDateTime | null;
  municipalRegistration?: string | null;
  persons?: (Person | null)[] | null;
  positions?: (Position | null)[] | null;
  schoolTypeId?: number | null;
  stateRegistration?: string | null;
}

export interface SchoolLevel {
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  levelId?: number | null;
  levelName?: number | null;
  modifiedAt?: NaiveDateTime | null;
  schoolId?: number | null;
}

export interface RelatedPerson {
  activationDate?: NaiveDateTime | null;
  birthDate?: Date | null;
  createdAt?: NaiveDateTime | null;
  email?: string | null;
  id?: string | null;
  isConfirmedRelation?: boolean | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
  person?: Person | null;
  positions?: (Position | null)[] | null;
  relatedPersonId?: number | null;
}

export interface Role {
  icon?: string | null;
  id?: string | null;
  name?: string | null;
  positions?: Position | null;
  sort?: number | null;
}

export interface UsedVoucher {
  campaingId?: number | null;
  campaingStatusId?: number | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  parentId?: number | null;
  personId?: number | null;
  schoolId?: number | null;
  voucherCode?: string | null;
}

export interface PositionStatus {
  icon?: string | null;
  id?: string | null;
  name?: string | null;
  sort?: number | null;
}

export interface PositionClassStatus {
  icon?: string | null;
  id?: string | null;
  name?: string | null;
  sort?: number | null;
}

export interface ClassProduct {
  adoption?: Adoption | null;
  adoptionId?: number | null;
  adoptionStatusId?: number | null;
  classId?: number | null;
  companyId?: number | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  isbn?: string | null;
  levelGrades?: (CollectionAdoptionLevelGrade | null)[] | null;
  modifiedAt?: NaiveDateTime | null;
  ownerId?: number | null;
  productId?: number | null;
  productTitle?: string | null;
  sku?: string | null;
}

export interface VoucherClass {
  campaingId?: number | null;
  campaingStatusId?: number | null;
  classId?: number | null;
  createdAt?: NaiveDateTime | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  parentId?: number | null;
  useLimit?: number | null;
  voucherCode?: string | null;
  voucherStatus?: VoucherStatus | null;
  voucherStatusId?: number | null;
}

export interface VoucherStatus {
  id?: string | null;
  name?: string | null;
}

export interface Level {
  createdAt?: NaiveDateTime | null;
  grades?: (Grade | null)[] | null;
  id?: string | null;
  modifiedAt?: NaiveDateTime | null;
  name?: string | null;
}

export interface Grade {
  id?: string | null;
  name?: string | null;
}
export interface ClassRootQueryTypeArgs {
  id: string;
}
export interface PersonRootQueryTypeArgs {
  id: string;
}
export interface SchoolRootQueryTypeArgs {
  id: string;
}
export interface VouchersClassArgs {
  voucherStatusId?: number | null;
}
export interface LevelGradesCollectionAdoptionArgs {
  gradeId?: number | null;
  levelId?: number | null;
}
export interface CollectionsAdoptionArgs {
  adoptionStatusId?: number | null;
  brandId?: number | null;
  collectionId?: number | null;
}
export interface ProductsAdoptionArgs {
  adoptionStatusId?: number | null;
  sku?: string | null;
}
export interface ServicesAdoptionArgs {
  adoptionStatusId?: number | null;
  applicationId?: number | null;
  serviceId?: number | null;
}
export interface GradesProductAdoptionArgs {
  gradeId?: number | null;
}
export interface LevelsProductAdoptionArgs {
  levelId?: number | null;
}
export interface LevelGradesServiceAdoptionArgs {
  gradeId?: number | null;
  levelId?: number | null;
}
export interface DisciplinesCollectionAdoptionLevelGradeArgs {
  disciplineId?: number | null;
}
export interface PositionClassesPositionArgs {
  classId?: number | null;
  positionClassStatusId?: number | null;
}
export interface PositionsPersonArgs {
  positionStatusId?: number | null;
  roleId?: number | null;
  schoolId?: number | null;
}
export interface AdoptionsSchoolArgs {
  year?: number | null;
}
export interface ClassesSchoolArgs {
  name?: string | null;
  periodId?: number | null;
  schoolYear?: number | null;
  status?: string | null;
}
export interface LevelsSchoolArgs {
  levelId?: number | null;
}
export interface PersonsSchoolArgs {
  id?: number | null;
  limit?: number | null;
  offset?: number | null;
}
export interface PositionsSchoolArgs {
  personId?: number | null;
  positionStatusId?: number | null;
  roleId?: number | null;
}
export interface PositionsRelatedPersonArgs {
  positionStatusId?: number | null;
  roleId?: number | null;
  schoolId?: number | null;
}
export interface LevelGradesClassProductArgs {
  gradeId?: number | null;
  levelId?: number | null;
}
export interface GradesLevelArgs {
  id?: number | null;
}

export namespace RootQueryTypeResolvers {
  export interface Resolvers<Context = any> {
    class?: ClassResolver<Class | null, any, Context> /** Get classes */;
    level?: LevelResolver<
      (Level | null)[] | null,
      any,
      Context
    > /** Get level grades */;
    period?: PeriodResolver<
      (Period | null)[] | null,
      any,
      Context
    > /** Get periods */;
    person?: PersonResolver<
      Person | null,
      any,
      Context
    > /** Get first person */;
    role?: RoleResolver<(Role | null)[] | null, any, Context> /** Get roles */;
    school?: SchoolResolver<School | null, any, Context> /** Get school */;
  }

  export type ClassResolver<
    R = Class | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, ClassArgs>;
  export interface ClassArgs {
    id: string;
  }

  export type LevelResolver<
    R = (Level | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PeriodResolver<
    R = (Period | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonResolver<
    R = Person | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PersonArgs>;
  export interface PersonArgs {
    id: string;
  }

  export type RoleResolver<
    R = (Role | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolResolver<
    R = School | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, SchoolArgs>;
  export interface SchoolArgs {
    id: string;
  }
}

export namespace ClassResolvers {
  export interface Resolvers<Context = any> {
    applicationId?: ApplicationIdResolver<number | null, any, Context>;
    awsId?: AwsIdResolver<string | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelGrades?: LevelGradesResolver<
      (ClassLevelGrade | null)[] | null,
      any,
      Context
    >;
    materials?: MaterialsResolver<
      (ClassCollectionLevelGrade | null)[] | null,
      any,
      Context
    >;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    namedVoucherUploaded?: NamedVoucherUploadedResolver<
      boolean | null,
      any,
      Context
    >;
    period?: PeriodResolver<Period | null, any, Context>;
    periodId?: PeriodIdResolver<number | null, any, Context>;
    positions?: PositionsResolver<
      (PositionClass | null)[] | null,
      any,
      Context
    >;
    products?: ProductsResolver<(ClassProduct | null)[] | null, any, Context>;
    school?: SchoolResolver<School | null, any, Context>;
    schoolId?: SchoolIdResolver<number | null, any, Context>;
    status?: StatusResolver<string | null, any, Context>;
    vouchers?: VouchersResolver<(VoucherClass | null)[] | null, any, Context>;
  }

  export type ApplicationIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AwsIdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelGradesResolver<
    R = (ClassLevelGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type MaterialsResolver<
    R = (ClassCollectionLevelGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NamedVoucherUploadedResolver<
    R = boolean | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PeriodResolver<
    R = Period | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PeriodIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionsResolver<
    R = (PositionClass | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductsResolver<
    R = (ClassProduct | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolResolver<
    R = School | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type StatusResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type VouchersResolver<
    R = (VoucherClass | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, VouchersArgs>;
  export interface VouchersArgs {
    voucherStatusId?: number | null;
  }
}

export namespace ClassLevelGradeResolvers {
  export interface Resolvers<Context = any> {
    classId?: ClassIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    gradeId?: GradeIdResolver<number | null, any, Context>;
    gradeName?: GradeNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
  }

  export type ClassIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ClassCollectionLevelGradeResolvers {
  export interface Resolvers<Context = any> {
    class?: ClassResolver<Class | null, any, Context>;
    classId?: ClassIdResolver<number | null, any, Context>;
    collection?: CollectionResolver<CollectionAdoption | null, any, Context>;
    collectionAdoptionId?: CollectionAdoptionIdResolver<
      number | null,
      any,
      Context
    >;
    collectionAdoptionLevelGradeId?: CollectionAdoptionLevelGradeIdResolver<
      number | null,
      any,
      Context
    >;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    disciplines?: DisciplinesResolver<
      (CollectionAdoptionLevelGradeDiscipline | null)[] | null,
      any,
      Context
    >;
    gradeId?: GradeIdResolver<number | null, any, Context>;
    gradeName?: GradeNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
  }

  export type ClassResolver<
    R = Class | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ClassIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionResolver<
    R = CollectionAdoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionAdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionAdoptionLevelGradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplinesResolver<
    R = (CollectionAdoptionLevelGradeDiscipline | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace CollectionAdoptionResolvers {
  export interface Resolvers<Context = any> {
    adoption?: AdoptionResolver<Adoption | null, any, Context>;
    adoptionId?: AdoptionIdResolver<number | null, any, Context>;
    adoptionStatus?: AdoptionStatusResolver<
      AdoptionStatus | null,
      any,
      Context
    >;
    adoptionStatusId?: AdoptionStatusIdResolver<number | null, any, Context>;
    brand?: BrandResolver<Brand | null, any, Context>;
    brandId?: BrandIdResolver<number | null, any, Context>;
    collection?: CollectionResolver<Collection | null, any, Context>;
    collectionId?: CollectionIdResolver<number | null, any, Context>;
    collectionName?: CollectionNameResolver<string | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelGrades?: LevelGradesResolver<
      (CollectionAdoptionLevelGrade | null)[] | null,
      any,
      Context
    >;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    ownerId?: OwnerIdResolver<number | null, any, Context>;
  }

  export type AdoptionResolver<
    R = Adoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusResolver<
    R = AdoptionStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type BrandResolver<
    R = Brand | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type BrandIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionResolver<
    R = Collection | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelGradesResolver<
    R = (CollectionAdoptionLevelGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, LevelGradesArgs>;
  export interface LevelGradesArgs {
    gradeId?: number | null;
    levelId?: number | null;
  }

  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OwnerIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace AdoptionResolvers {
  export interface Resolvers<Context = any> {
    adoptionStatus?: AdoptionStatusResolver<
      AdoptionStatus | null,
      any,
      Context
    >;
    adoptionStatusId?: AdoptionStatusIdResolver<number | null, any, Context>;
    collections?: CollectionsResolver<
      (CollectionAdoption | null)[] | null,
      any,
      Context
    >;
    id?: IdResolver<string | null, any, Context>;
    ownerId?: OwnerIdResolver<number | null, any, Context>;
    products?: ProductsResolver<
      (ProductAdoption | null)[] | null,
      any,
      Context
    >;
    schoolId?: SchoolIdResolver<number | null, any, Context>;
    services?: ServicesResolver<
      (ServiceAdoption | null)[] | null,
      any,
      Context
    >;
    year?: YearResolver<number | null, any, Context>;
  }

  export type AdoptionStatusResolver<
    R = AdoptionStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionsResolver<
    R = (CollectionAdoption | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, CollectionsArgs>;
  export interface CollectionsArgs {
    adoptionStatusId?: number | null;
    brandId?: number | null;
    collectionId?: number | null;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OwnerIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductsResolver<
    R = (ProductAdoption | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, ProductsArgs>;
  export interface ProductsArgs {
    adoptionStatusId?: number | null;
    sku?: string | null;
  }

  export type SchoolIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ServicesResolver<
    R = (ServiceAdoption | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, ServicesArgs>;
  export interface ServicesArgs {
    adoptionStatusId?: number | null;
    applicationId?: number | null;
    serviceId?: number | null;
  }

  export type YearResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace AdoptionStatusResolvers {
  export interface Resolvers<Context = any> {
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ProductAdoptionResolvers {
  export interface Resolvers<Context = any> {
    adoption?: AdoptionResolver<Adoption | null, any, Context>;
    adoptionId?: AdoptionIdResolver<number | null, any, Context>;
    adoptionStatus?: AdoptionStatusResolver<
      AdoptionStatus | null,
      any,
      Context
    >;
    adoptionStatusId?: AdoptionStatusIdResolver<number | null, any, Context>;
    companyId?: CompanyIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    grades?: GradesResolver<(ProductGrade | null)[] | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    isbn?: IsbnResolver<number | null, any, Context>;
    levels?: LevelsResolver<(ProductLevel | null)[] | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    ownerId?: OwnerIdResolver<number | null, any, Context>;
    productId?: ProductIdResolver<number | null, any, Context>;
    productTitle?: ProductTitleResolver<number | null, any, Context>;
    sku?: SkuResolver<string | null, any, Context>;
  }

  export type AdoptionResolver<
    R = Adoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusResolver<
    R = AdoptionStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CompanyIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradesResolver<
    R = (ProductGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, GradesArgs>;
  export interface GradesArgs {
    gradeId?: number | null;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IsbnResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelsResolver<
    R = (ProductLevel | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, LevelsArgs>;
  export interface LevelsArgs {
    levelId?: number | null;
  }

  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OwnerIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductTitleResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SkuResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ProductGradeResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    gradeId?: GradeIdResolver<number | null, any, Context>;
    gradeName?: GradeNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    productId?: ProductIdResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductIdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ProductLevelResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    productId?: ProductIdResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductIdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ServiceAdoptionResolvers {
  export interface Resolvers<Context = any> {
    adoption?: AdoptionResolver<Adoption | null, any, Context>;
    adoptionId?: AdoptionIdResolver<number | null, any, Context>;
    adoptionStatus?: AdoptionStatusResolver<
      AdoptionStatus | null,
      any,
      Context
    >;
    adoptionStatusId?: AdoptionStatusIdResolver<number | null, any, Context>;
    application?: ApplicationResolver<Application | null, any, Context>;
    applicationId?: ApplicationIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelGrades?: LevelGradesResolver<
      (CollectionAdoptionLevelGrade | null)[] | null,
      any,
      Context
    >;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    ownerId?: OwnerIdResolver<number | null, any, Context>;
    service?: ServiceResolver<Service | null, any, Context>;
    serviceId?: ServiceIdResolver<number | null, any, Context>;
    serviceName?: ServiceNameResolver<string | null, any, Context>;
    url?: UrlResolver<string | null, any, Context>;
  }

  export type AdoptionResolver<
    R = Adoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusResolver<
    R = AdoptionStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ApplicationResolver<
    R = Application | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ApplicationIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelGradesResolver<
    R = (CollectionAdoptionLevelGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, LevelGradesArgs>;
  export interface LevelGradesArgs {
    gradeId?: number | null;
    levelId?: number | null;
  }

  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OwnerIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ServiceResolver<
    R = Service | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ServiceIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ServiceNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type UrlResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ApplicationResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    url?: UrlResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type UrlResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace CollectionAdoptionLevelGradeResolvers {
  export interface Resolvers<Context = any> {
    collectionAdoption?: CollectionAdoptionResolver<
      CollectionAdoption | null,
      any,
      Context
    >;
    collectionAdoptionId?: CollectionAdoptionIdResolver<
      number | null,
      any,
      Context
    >;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    disciplines?: DisciplinesResolver<
      (CollectionAdoptionLevelGradeDiscipline | null)[] | null,
      any,
      Context
    >;
    gradeId?: GradeIdResolver<number | null, any, Context>;
    gradeName?: GradeNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
  }

  export type CollectionAdoptionResolver<
    R = CollectionAdoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionAdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplinesResolver<
    R = (CollectionAdoptionLevelGradeDiscipline | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, DisciplinesArgs>;
  export interface DisciplinesArgs {
    disciplineId?: number | null;
  }

  export type GradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradeNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace CollectionAdoptionLevelGradeDisciplineResolvers {
  export interface Resolvers<Context = any> {
    collectionAdoptionLevelGrade?: CollectionAdoptionLevelGradeResolver<
      CollectionAdoptionLevelGrade | null,
      any,
      Context
    >;
    collectionAdoptionLevelGradeId?: CollectionAdoptionLevelGradeIdResolver<
      number | null,
      any,
      Context
    >;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    curricularComponentId?: CurricularComponentIdResolver<
      number | null,
      any,
      Context
    >;
    curricularComponentName?: CurricularComponentNameResolver<
      string | null,
      any,
      Context
    >;
    disciplineId?: DisciplineIdResolver<number | null, any, Context>;
    disciplineName?: DisciplineNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
  }

  export type CollectionAdoptionLevelGradeResolver<
    R = CollectionAdoptionLevelGrade | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CollectionAdoptionLevelGradeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CurricularComponentIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CurricularComponentNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplineIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplineNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ServiceResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    idApplication?: IdApplicationResolver<number | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    url?: UrlResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdApplicationResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type UrlResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace BrandResolvers {
  export interface Resolvers<Context = any> {
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace CollectionResolvers {
  export interface Resolvers<Context = any> {
    codeCollection?: CodeCollectionResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type CodeCollectionResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PeriodResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    endAt?: EndAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    periodStatusId?: PeriodStatusIdResolver<number | null, any, Context>;
    startAt?: StartAtResolver<NaiveDateTime | null, any, Context>;
    year?: YearResolver<number | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type EndAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PeriodStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type StartAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type YearResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionClassResolvers {
  export interface Resolvers<Context = any> {
    class?: ClassResolver<Class | null, any, Context>;
    classId?: ClassIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    endAt?: EndAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    position?: PositionResolver<Position | null, any, Context>;
    positionClassStatusId?: PositionClassStatusIdResolver<
      number | null,
      any,
      Context
    >;
    positionId?: PositionIdResolver<number | null, any, Context>;
    startAt?: StartAtResolver<NaiveDateTime | null, any, Context>;
    status?: StatusResolver<PositionClassStatus | null, any, Context>;
  }

  export type ClassResolver<
    R = Class | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ClassIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type EndAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionResolver<
    R = Position | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionClassStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type StartAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type StatusResolver<
    R = PositionClassStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levels?: LevelsResolver<(PositionLevel | null)[] | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    person?: PersonResolver<Person | null, any, Context>;
    personId?: PersonIdResolver<number | null, any, Context>;
    positionClasses?: PositionClassesResolver<
      (PositionClass | null)[] | null,
      any,
      Context
    >;
    positionStatus?: PositionStatusResolver<
      PositionStatus | null,
      any,
      Context
    >;
    positionStatusId?: PositionStatusIdResolver<number | null, any, Context>;
    role?: RoleResolver<Role | null, any, Context>;
    roleId?: RoleIdResolver<number | null, any, Context>;
    school?: SchoolResolver<School | null, any, Context>;
    schoolId?: SchoolIdResolver<number | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelsResolver<
    R = (PositionLevel | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonResolver<
    R = Person | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionClassesResolver<
    R = (PositionClass | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PositionClassesArgs>;
  export interface PositionClassesArgs {
    classId?: number | null;
    positionClassStatusId?: number | null;
  }

  export type PositionStatusResolver<
    R = PositionStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type RoleResolver<
    R = Role | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type RoleIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolResolver<
    R = School | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionLevelResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    disciplines?: DisciplinesResolver<
      (PositionLevelDiscipline | null)[] | null,
      any,
      Context
    >;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    positionId?: PositionIdResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplinesResolver<
    R = (PositionLevelDiscipline | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionIdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionLevelDisciplineResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    disciplineId?: DisciplineIdResolver<number | null, any, Context>;
    disciplineName?: DisciplineNameResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    positionLevelId?: PositionLevelIdResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplineIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type DisciplineNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionLevelIdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PersonResolvers {
  export interface Resolvers<Context = any> {
    activationCode?: ActivationCodeResolver<string | null, any, Context>;
    activationDate?: ActivationDateResolver<NaiveDateTime | null, any, Context>;
    birthDate?: BirthDateResolver<Date | null, any, Context>;
    cpf?: CpfResolver<string | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    email?: EmailResolver<string | null, any, Context>;
    enrollment?: EnrollmentResolver<string | null, any, Context>;
    gender?: GenderResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    inactivationDate?: InactivationDateResolver<
      NaiveDateTime | null,
      any,
      Context
    >;
    isResponsableCpf?: IsResponsableCpfResolver<number | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    optionInEmail?: OptionInEmailResolver<string | null, any, Context>;
    optionInSms?: OptionInSmsResolver<string | null, any, Context>;
    optionOfUse?: OptionOfUseResolver<string | null, any, Context>;
    parentId?: ParentIdResolver<number | null, any, Context>;
    pessoa?: PessoaResolver<Pessoa | null, any, Context>;
    positions?: PositionsResolver<(Position | null)[] | null, any, Context>;
    relatedPersons?: RelatedPersonsResolver<
      (RelatedPerson | null)[] | null,
      any,
      Context
    >;
    rg?: RgResolver<string | null, any, Context>;
    role?: RoleResolver<Role | null, any, Context>;
    usedVouchers?: UsedVouchersResolver<
      (UsedVoucher | null)[] | null,
      any,
      Context
    >;
  }

  export type ActivationCodeResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ActivationDateResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type BirthDateResolver<
    R = Date | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CpfResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type EmailResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type EnrollmentResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GenderResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type InactivationDateResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IsResponsableCpfResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OptionInEmailResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OptionInSmsResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OptionOfUseResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ParentIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PessoaResolver<
    R = Pessoa | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionsResolver<
    R = (Position | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PositionsArgs>;
  export interface PositionsArgs {
    positionStatusId?: number | null;
    roleId?: number | null;
    schoolId?: number | null;
  }

  export type RelatedPersonsResolver<
    R = (RelatedPerson | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type RgResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type RoleResolver<
    R = Role | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type UsedVouchersResolver<
    R = (UsedVoucher | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PessoaResolvers {
  export interface Resolvers<Context = any> {
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    person?: PersonResolver<Person | null, any, Context>;
    school?: SchoolResolver<School | null, any, Context>;
    syncAt?: SyncAtResolver<NaiveDateTime | null, any, Context>;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonResolver<
    R = Person | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolResolver<
    R = School | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SyncAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace SchoolResolvers {
  export interface Resolvers<Context = any> {
    adoptions?: AdoptionsResolver<(Adoption | null)[] | null, any, Context>;
    classes?: ClassesResolver<(Class | null)[] | null, any, Context>;
    cnpj?: CnpjResolver<string | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    legalName?: LegalNameResolver<string | null, any, Context>;
    levels?: LevelsResolver<(SchoolLevel | null)[] | null, any, Context>;
    mecCode?: MecCodeResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    municipalRegistration?: MunicipalRegistrationResolver<
      string | null,
      any,
      Context
    >;
    persons?: PersonsResolver<(Person | null)[] | null, any, Context>;
    positions?: PositionsResolver<(Position | null)[] | null, any, Context>;
    schoolTypeId?: SchoolTypeIdResolver<number | null, any, Context>;
    stateRegistration?: StateRegistrationResolver<string | null, any, Context>;
  }

  export type AdoptionsResolver<
    R = (Adoption | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, AdoptionsArgs>;
  export interface AdoptionsArgs {
    year?: number | null;
  }

  export type ClassesResolver<
    R = (Class | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, ClassesArgs>;
  export interface ClassesArgs {
    name?: string | null;
    periodId?: number | null;
    schoolYear?: number | null;
    status?: string | null;
  }

  export type CnpjResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LegalNameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelsResolver<
    R = (SchoolLevel | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, LevelsArgs>;
  export interface LevelsArgs {
    levelId?: number | null;
  }

  export type MecCodeResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type MunicipalRegistrationResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonsResolver<
    R = (Person | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PersonsArgs>;
  export interface PersonsArgs {
    id?: number | null;
    limit?: number | null;
    offset?: number | null;
  }

  export type PositionsResolver<
    R = (Position | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PositionsArgs>;
  export interface PositionsArgs {
    personId?: number | null;
    positionStatusId?: number | null;
    roleId?: number | null;
  }

  export type SchoolTypeIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type StateRegistrationResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace SchoolLevelResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    levelId?: LevelIdResolver<number | null, any, Context>;
    levelName?: LevelNameResolver<number | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    schoolId?: SchoolIdResolver<number | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelNameResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace RelatedPersonResolvers {
  export interface Resolvers<Context = any> {
    activationDate?: ActivationDateResolver<NaiveDateTime | null, any, Context>;
    birthDate?: BirthDateResolver<Date | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    email?: EmailResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    isConfirmedRelation?: IsConfirmedRelationResolver<
      boolean | null,
      any,
      Context
    >;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    person?: PersonResolver<Person | null, any, Context>;
    positions?: PositionsResolver<(Position | null)[] | null, any, Context>;
    relatedPersonId?: RelatedPersonIdResolver<number | null, any, Context>;
  }

  export type ActivationDateResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type BirthDateResolver<
    R = Date | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type EmailResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IsConfirmedRelationResolver<
    R = boolean | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonResolver<
    R = Person | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionsResolver<
    R = (Position | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, PositionsArgs>;
  export interface PositionsArgs {
    positionStatusId?: number | null;
    roleId?: number | null;
    schoolId?: number | null;
  }

  export type RelatedPersonIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace RoleResolvers {
  export interface Resolvers<Context = any> {
    icon?: IconResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    positions?: PositionsResolver<Position | null, any, Context>;
    sort?: SortResolver<number | null, any, Context>;
  }

  export type IconResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PositionsResolver<
    R = Position | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SortResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace UsedVoucherResolvers {
  export interface Resolvers<Context = any> {
    campaingId?: CampaingIdResolver<number | null, any, Context>;
    campaingStatusId?: CampaingStatusIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    parentId?: ParentIdResolver<number | null, any, Context>;
    personId?: PersonIdResolver<number | null, any, Context>;
    schoolId?: SchoolIdResolver<number | null, any, Context>;
    voucherCode?: VoucherCodeResolver<string | null, any, Context>;
  }

  export type CampaingIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CampaingStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ParentIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type PersonIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SchoolIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type VoucherCodeResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionStatusResolvers {
  export interface Resolvers<Context = any> {
    icon?: IconResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    sort?: SortResolver<number | null, any, Context>;
  }

  export type IconResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SortResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace PositionClassStatusResolvers {
  export interface Resolvers<Context = any> {
    icon?: IconResolver<string | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
    sort?: SortResolver<number | null, any, Context>;
  }

  export type IconResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SortResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace ClassProductResolvers {
  export interface Resolvers<Context = any> {
    adoption?: AdoptionResolver<Adoption | null, any, Context>;
    adoptionId?: AdoptionIdResolver<number | null, any, Context>;
    adoptionStatusId?: AdoptionStatusIdResolver<number | null, any, Context>;
    classId?: ClassIdResolver<number | null, any, Context>;
    companyId?: CompanyIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    isbn?: IsbnResolver<string | null, any, Context>;
    levelGrades?: LevelGradesResolver<
      (CollectionAdoptionLevelGrade | null)[] | null,
      any,
      Context
    >;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    ownerId?: OwnerIdResolver<number | null, any, Context>;
    productId?: ProductIdResolver<number | null, any, Context>;
    productTitle?: ProductTitleResolver<string | null, any, Context>;
    sku?: SkuResolver<string | null, any, Context>;
  }

  export type AdoptionResolver<
    R = Adoption | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type AdoptionStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ClassIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CompanyIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IsbnResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type LevelGradesResolver<
    R = (CollectionAdoptionLevelGrade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, LevelGradesArgs>;
  export interface LevelGradesArgs {
    gradeId?: number | null;
    levelId?: number | null;
  }

  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type OwnerIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ProductTitleResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type SkuResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace VoucherClassResolvers {
  export interface Resolvers<Context = any> {
    campaingId?: CampaingIdResolver<number | null, any, Context>;
    campaingStatusId?: CampaingStatusIdResolver<number | null, any, Context>;
    classId?: ClassIdResolver<number | null, any, Context>;
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    parentId?: ParentIdResolver<number | null, any, Context>;
    useLimit?: UseLimitResolver<number | null, any, Context>;
    voucherCode?: VoucherCodeResolver<string | null, any, Context>;
    voucherStatus?: VoucherStatusResolver<VoucherStatus | null, any, Context>;
    voucherStatusId?: VoucherStatusIdResolver<number | null, any, Context>;
  }

  export type CampaingIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CampaingStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ClassIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ParentIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type UseLimitResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type VoucherCodeResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type VoucherStatusResolver<
    R = VoucherStatus | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type VoucherStatusIdResolver<
    R = number | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace VoucherStatusResolvers {
  export interface Resolvers<Context = any> {
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace LevelResolvers {
  export interface Resolvers<Context = any> {
    createdAt?: CreatedAtResolver<NaiveDateTime | null, any, Context>;
    grades?: GradesResolver<(Grade | null)[] | null, any, Context>;
    id?: IdResolver<string | null, any, Context>;
    modifiedAt?: ModifiedAtResolver<NaiveDateTime | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type CreatedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type GradesResolver<
    R = (Grade | null)[] | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context, GradesArgs>;
  export interface GradesArgs {
    id?: number | null;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type ModifiedAtResolver<
    R = NaiveDateTime | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace GradeResolvers {
  export interface Resolvers<Context = any> {
    id?: IdResolver<string | null, any, Context>;
    name?: NameResolver<string | null, any, Context>;
  }

  export type IdResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
  export type NameResolver<
    R = string | null,
    Parent = any,
    Context = any
  > = Resolver<R, Parent, Context>;
}

export namespace SchoolQuery {
  export type Variables = {};

  export type Query = {
    __typename?: "Query";
    school?: School | null;
  };

  export type School = {
    __typename?: "School";
    legalName?: string | null;
    cnpj?: string | null;
    id?: string | null;
    persons?: (Persons | null)[] | null;
    classes?: (Classes | null)[] | null;
  };

  export type Persons = {
    __typename?: "Person";
    id?: string | null;
  };

  export type Classes = {
    __typename?: "Class";
    id?: string | null;
    name?: string | null;
    vouchers?: (Vouchers | null)[] | null;
  };

  export type Vouchers = {
    __typename?: "VoucherClass";
    classId?: number | null;
    voucherCode?: string | null;
    voucherStatusId?: number | null;
    voucherStatus?: VoucherStatus | null;
  };

  export type VoucherStatus = {
    __typename?: "VoucherStatus";
    id?: string | null;
    name?: string | null;
  };
}
