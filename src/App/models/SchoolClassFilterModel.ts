export default class SchoolClassFilterModel {
  public years : any
  public levels : any
  public grades : any
  public schoolCollections : any
  public schoolClasses : any
}
