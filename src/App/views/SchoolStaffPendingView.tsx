import * as React from 'react';
import { Title } from 'plurall-texts'
import { Button } from 'plurall-button'
import Modal from '../components/common/Modal'
import Dropdown from 'plurall-dropdown'
import Grid from '../containers/Grid'
import Row from '../containers/Row'
import DataTable from '../components/common/DataTable'
import StudentParentServices from '../services/StudentParentServices'
import Pagination from '../components/common/Pagination'
import SchoolStaffSpreadsheetInviteBodyView from './Modals/SchoolStaffSpreadsheetInviteBodyView'
import SchoolStaffSystemInviteBodyView from './Modals/SchoolStaffSystemInviteBodyView'
import SchoolStaffPermissionsBodyView from './Modals/SchoolStaffPermissionsBodyView'

interface IState {
  modal: any,
  pendingUsers: any,
  rows: any,
  currentPage: any,
  nameUserModal: any,
  emailUserModal: any,
  dataHeaders: any,
  checkedRows?: any,
  totalRecords?: any
}

class SchoolStaffPendingView extends React.Component<any, IState> {
  public ModalEnum = {
    APPROVE: 1,
    DISAPPROVE: 2,
    INVITE_USER_SPREADSHEET: 3,
    INVITE_USER_SYSTEM: 4,
    PERMISSIONS: 5,
  }

  public permissions = {
    profiles: {
      director: true,
      keeper: true,
      coordinator: true,
      teacher: true,
    },
    levels: {
      coordinator: [],
      teacher: ['infantil'],
    },
    teacherDisciplines: [
      {
        infantil: [
          {
            value: 1,
            label: 'Português',
          },
        ],
      },
    ],
    teacherSchoolClasses: [
      {
        infantil: [
          {
            value: 1,
            label: 'Manhã',
          },
        ],
      },
    ],
  }

  public levels = [
    {
      label: 'Ensino Infantil',
      value: 'infantil',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Fundamental 1',
      value: 'fund1',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
        { label: 'Portugues', value: 3 },
        { label: 'Matemática', value: 4 },
        { label: 'Portugues', value: 5 },
        { label: 'Matemática', value: 6 },
        { label: 'Portugues', value: 7 },
        { label: 'Matemática', value: 8 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Fundamental 2',
      value: 'fund2',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Médio',
      value: 'medio',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Pré-vestivular',
      value: 'pre',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
  ]

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    this.getUsers()
  }

  public getInitialState() {
    return {
      modal: false,
      pendingUsers: 31,
      rows: [],
      currentPage: 1,
      nameUserModal: '',
      emailUserModal: '',
      dataHeaders: [
        'Perfil/Nome',
        'Status',
        'E-mail',
        'Turma',
        'Disciplinas',
        '',
      ],
    }
  }

  public bindHandlers() {
    this.handleChangeInvite = this.handleChangeInvite.bind(this)
    this.changeRow = this.changeRow.bind(this)
    this.changePage = this.changePage.bind(this)
  }

  public handleChangeInvite(value) {
    this.showModal(value)
  }

  public showModal(modal, name = null, email = null) {
    this.setState({ modal: modal })
    this.setState({ nameUserModal: name })
    this.setState({ emailUserModal: email })
  }

  public closeModal() {
    this.setState({ modal: false })
  }

  public changeRow(value) {
    this.setState({ checkedRows: value })
  }

  public changePage(page) {
    this.setState({ currentPage: parseInt(page) })
  }

  public async getUsers() {
    const page = this.state.currentPage
    const items = await StudentParentServices.list(page, 0)
    const rows = items['users'].map(item => {
      return [
        item.name,
        item.status,
        item.email,
        item.schoolClasses.map(c => {
          return c.description
        }),
        item.disciplines.map((c, index) => {
          if (index !== item.disciplines.length - 1) {
            return c.name + ','
          } else {
            return c.name
          }
        }),
        <Button
          key=""
          type="primary"
          onClick={() =>
            this.showModal(this.ModalEnum.PERMISSIONS, item.name, item.email)
          }
        >
          Gerenciar
        </Button>,
      ]
    })
    this.setState({ rows: rows })
    this.setState({ totalRecords: parseInt(items['totalRecords']) })
  }

  public render() {
    return (
      <div>
        <Title>Usuários Pendentes de Aprovação</Title>
        <Row>
          <Grid cols="8 8 8 8">
            <Title size="medium">
              Você tem {this.state.pendingUsers} usuários pendentes de
              aprovação.
            </Title>
            <Title size="small">
              Você pode gerenciar as permissões do usuário antes de aprová-lo.
            </Title>
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 8">
            <Button
              type="primary"
              color="#666666"
              border="#655aa3"
              background="#FFFFFF"
              onClick={() => this.showModal(this.ModalEnum.APPROVE)}
            >
              Aprovar Usuários
            </Button>
            <Button
              type="primary"
              color="#666666"
              border="#655aa3"
              background="#FFFFFF"
              onClick={() => this.showModal(this.ModalEnum.DISAPPROVE)}
            >
              Reprovar Usuários
            </Button>
          </Grid>
          <Grid cols="12 12 12 4">
            <Dropdown
              name="invite_users"
              options={[
                { label: 'CONVIDAR EQUIPE ESCOLAR', value: '' },
                {
                  label: 'Convidar via sistema',
                  value: this.ModalEnum.INVITE_USER_SYSTEM,
                },
                {
                  label: 'Convidar professores via planilha',
                  value: this.ModalEnum.INVITE_USER_SPREADSHEET,
                },
              ]}
              onChange={this.handleChangeInvite}
            />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <DataTable
              headers={this.state.dataHeaders}
              items={this.state.rows}
              rowCheckbox={true}
              onChangeCheckBox={this.changeRow}
            />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <Pagination
              totalRecords={this.state.totalRecords}
              recordsPerPage={10}
              currentPage={this.state.currentPage}
              callbackView={this.changePage}
            />
          </Grid>
        </Row>
        <Modal
          show={this.state.modal === this.ModalEnum.APPROVE}
          title="Aprovação de usuários"
          subTitle="Lista de usuários que serão aprovados"
          width={900}
          onClose={() => this.closeModal()}
        />
        <Modal
          show={this.state.modal === this.ModalEnum.DISAPPROVE}
          title="Reprovar usuários"
          subTitle="Lista de usuários que serão reprovados"
          width={900}
          onClose={() => this.closeModal()}
        />
        <div />
        <Modal
          show={this.state.modal === this.ModalEnum.INVITE_USER_SYSTEM}
          title="Criar usuário - Equipe Escolar"
          subTitle="Defina as permissões desse usuário"
          width={900}
          onClose={() => this.closeModal()}
        >
          <SchoolStaffSystemInviteBodyView levels={this.levels} />
        </Modal>

        <Modal
          show={this.state.modal === this.ModalEnum.INVITE_USER_SPREADSHEET}
          title="Convidar Professores via Planilha"
          subTitle="Envio de convites para PROFESSORES"
          width={900}
          onClose={() => this.closeModal()}
        >
          <SchoolStaffSpreadsheetInviteBodyView />
        </Modal>
        <Modal
          show={this.state.modal === this.ModalEnum.PERMISSIONS}
          title="Editar permissões do usuário"
          subTitle={
            'Você está editando o usuário: ' +
            this.state.nameUserModal +
            ' (' +
            this.state.emailUserModal +
            ')'
          }
          width={900}
          onClose={() => this.closeModal()}
        >
          <SchoolStaffPermissionsBodyView
            userPermissions={this.permissions}
            levels={this.levels}
          />
        </Modal>
      </div>
    )
  }
}

export default SchoolStaffPendingView
