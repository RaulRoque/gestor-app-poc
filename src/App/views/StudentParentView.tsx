import * as React from 'react';

import StudentParentFilter from '../components/common/filters/StudentParentFilter'
import StudentParentServices from '../services/StudentParentServices'
import DataTable from '../components/common/DataTable'
import { Button } from 'plurall-button'

import Grid from '../containers/Grid'
import Row from '../containers/Row'
import Pagination from '../components/common/Pagination'

interface IState {
  rows: any
  filters: any
  currentPage: any
  dataHeaders: any
  totalRecords: any
}

class StudentParentView extends React.Component<any, IState> {
  public handleClickFilter: any;

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }
  public getInitialState() {
    return {
      rows: [],
      filters: [],
      currentPage: 1,
      dataHeaders: ['Perfil/Nome', 'Status', 'Login', 'Turma', ''],
      totalRecords: 0
    }
  }
  public bindHandlers() {
    this.searchSchoolClasses = this.searchSchoolClasses.bind(this)
    this.changePage = this.changePage.bind(this)
  }

  public changePage(page) {
    this.setState({ currentPage: parseInt(page) })
    this.fillTable()
  }

  public searchSchoolClasses() {
    this.fillTable()
  }

  public async fillTable() {
    this.setState({ rows: [] })
    const filters = this.state.filters
    const page = this.state.currentPage
    const items = await StudentParentServices.list(filters, page)

    const rows = items['users'].map(item => {
      return [
        item.name,
        item.status,
        item.email,
        item.schoolClasses.map(c => {
          return c.description
        }),
        <Button key="" type="primary" onClick={this.handleClickFilter}>
          Gerenciar
        </Button>,
      ]
    })
    this.setState({ rows: rows })
    this.setState({ totalRecords: parseInt(items['totalRecords']) })
  }

  public render() {
    return (
      <div>
        <StudentParentFilter callbackView={this.searchSchoolClasses} />

        <Row>
          <Grid cols="12 12 12 12">
            <DataTable
              items={this.state.rows}
              headers={this.state.dataHeaders}
            />
          </Grid>
        </Row>

        <Row>
          <Grid cols="12 12 12 12">
            <Pagination
              totalRecords={this.state.totalRecords}
              recordsPerPage={10}
              currentPage={this.state.currentPage}
              callbackView={this.changePage}
            />
          </Grid>
        </Row>
      </div>
    )
  }
}

export default StudentParentView
