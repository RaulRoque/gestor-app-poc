import * as React from 'react';

import SchoolClassManagerFilter from '../components/common/filters/SchoolClassManagerFilter'
import UserService from '../services/UserService'
import DataTable from '../components/common/DataTable'
import Modal from '../components/common/Modal'
import Grid from '../containers/Grid'
import Row from '../containers/Row'
import { Button } from 'plurall-button'
import { Title } from 'plurall-texts'
import RemoveUsersBodyView from './Modals/RemoveUsersBodyView'
import ClassManagerBodyView from './Modals/ClassManagerBodyView'
import TeachersManagerClassBodyView from './Modals/TeachersManagerClassBodyView'
import MigrationUserBodyView from './Modals/MigrationUserBodyView'

interface IState {
  modal?: any,
  pendingUsers?: any,
  rows?: any,
  currentPage?: any,
  nameUserModal?: any,
  totalRecords?: any,
  dataHeaders?: any,
  checkedRows?: any,
  usersSelected?: any,
  users?: any,
  filters?: any
}

class SchoolClassManagerView extends React.Component<any, IState> {
  public ModalEnum = {
    TEACHER_MANAGER: 1,
    USERS_MIGRATION: 2,
    USERS_REMOVE: 3,
    CLASS_MANAGER: 4,
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    Object.freeze(this.ModalEnum)
  }

  public getInitialState() {
    return {
      rows: [],
      users: [],
      usersSelected: [],
      currentPage: 1,
      dataHeaders: ['Nome', 'Login', 'Data de Cadastro'],
    }
  }

  public bindHandlers() {
    this.searchSchoolClasses = this.searchSchoolClasses.bind(this)
    this.changePage = this.changePage.bind(this)
    this.changeRow = this.changeRow.bind(this)
  }

  public showModal(modal) {
    this.usersSelected()
    this.setState({ modal: modal })
  }
  public closeModal() {
    this.setState({ modal: false })
  }
  public changePage(page) {
    this.setState({ currentPage: parseInt(page) })
    this.fillTable()
  }

  public changeRow(value) {
    this.setState({ checkedRows: value })
  }

  public usersSelected() {
    this.setState({ usersSelected: [] })
    if (this.state.checkedRows) {
      const usersSelected = []
      this.state.checkedRows.forEach(row => {
        usersSelected.push(this.state.users[row])
      })
      this.setState({ usersSelected: usersSelected })
    }
  }

  public searchSchoolClasses() {
    this.fillTable()
  }

  public async fillTable() {
    this.setState({ rows: [] })
    this.setState({ users: [] })
    const filters = this.state.filters
    const page = this.state.currentPage
    const items = await UserService.list(filters, page)
    const rows = items['users'].map(item => {
      this.state.users.push({ value: item.id, label: item.name })
      return [
        <div key="">
          <div>
            {item.profiles.map(p => {
              return <strong key="">{p}</strong>
            })}
          </div>
          <div>{item.name}</div>
        </div>,
        item.username,
        item.createdAt,
      ]
    })
    this.setState({ rows: rows })
    this.setState({ totalRecords: parseInt(items['totalRecords']) })
  }
  public render() {
    return (
      <div>
        <Title>Gerenciar Turma</Title>
        <Row>
          <Grid cols="12 12 12 12">
            <Button
              onClick={() => this.showModal(this.ModalEnum.TEACHER_MANAGER)}
              type="default"
            >
              Gerenciar Professores
            </Button>

            <Button
              type="primary"
              color="#666666"
              border="#655aa3"
              background="#FFF"
              onClick={() => this.showModal(this.ModalEnum.USERS_MIGRATION)}
            >
              Migrar Usuários entre Turmas
            </Button>

            <Button
              type="primary"
              color="#666666"
              border="#655aa3"
              background="#FFF"
              onClick={() => this.showModal(this.ModalEnum.USERS_REMOVE)}
            >
              Remover Usuários
            </Button>

            <Button
              type="default"
              onClick={() => this.showModal(this.ModalEnum.CLASS_MANAGER)}
            >
              Gerenciar
            </Button>
          </Grid>
        </Row>

        <SchoolClassManagerFilter onFilter={this.searchSchoolClasses} />

        <Row>
          <Grid cols="12 12 12 12">
            <DataTable
              rowCheckbox={true}
              items={this.state.rows}
              headers={this.state.dataHeaders}
              onChangeCheckBox={this.changeRow}
            />
          </Grid>
        </Row>
        <Modal
          show={this.state.modal === this.ModalEnum.TEACHER_MANAGER}
          title="Gerenciar professores dessa turma"
          subTitle="Selecione abaixo os professores que deseja associar a esta turma"
          width={1000}
          onClose={() => this.closeModal()}
        >
          <TeachersManagerClassBodyView />
        </Modal>
        <Modal
          show={this.state.modal === this.ModalEnum.USERS_MIGRATION}
          title="Migrar usuários entre turmas"
          subTitle="Selecione abaixo os usuários que deseja migrar para outra turma"
          width={1000}
          onClose={() => this.closeModal()}
        >
          <MigrationUserBodyView usersSelected={this.state.usersSelected} />
        </Modal>
        <Modal
          show={this.state.modal === this.ModalEnum.USERS_REMOVE}
          title="Remover usuários dessa turma"
          subTitle="Os usuários abaixo serão removidos da turma, mas ainda poderão ser encontradas na lista de usuários"
          width={1000}
          onClose={() => this.closeModal()}
          labelSaveButton="Remover Usuários"
        >
          <RemoveUsersBodyView usersSelected={this.state.usersSelected} />
        </Modal>

        <Modal
          show={this.state.modal === this.ModalEnum.CLASS_MANAGER}
          title="Gerenciar"
          subTitle="Abaixo você consegue alterar o nome da turma e também alterar os materiais que estão associados a essa turma, segmento e ano."
          width={1000}
          onClose={() => this.closeModal()}
        >
          <ClassManagerBodyView />
        </Modal>
      </div>
    )
  }
}

export default SchoolClassManagerView
