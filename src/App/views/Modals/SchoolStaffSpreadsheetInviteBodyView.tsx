import * as React from 'react';
import { Text } from 'plurall-texts'
import { Input } from 'plurall-form'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import CheckboxGroup from '../../components/common/CheckboxGroup'
import Row from '../../containers/Row'
import { Button } from 'plurall-button'
import { FileUpload } from '../../components/common/FileUpload'
import { CSSProperties } from 'react';

interface IState {
  professores?: any
  professoresSelecionados?: any
}

export default class SchoolStaffSpreadsheetInviteBodyView extends React.Component<any, IState> {

  public labelStyle: CSSProperties = {
    display: 'block',
    fontSize: '16px',
    color: '#293A4C',
    fontWeight: 400,
  }
  public pStyle: CSSProperties = {
    display: 'block',
    fontSize: '16px',
    margin: '0 0 10px 0',
    color: '#293A4C',
    fontWeight: 300,
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {}
  }

  public bindHandlers() {
    this.removeProfessorSelecionado = this.removeProfessorSelecionado.bind(this)
    this.handleChangeProfessor = this.handleChangeProfessor.bind(this)
  }

  public handleChangeProfessor(value) {
    if (value !== '') {
      this.state.professores.forEach((professor, index) => {
        this.validateProfessor(index, professor, value)
      })
    }
  }

  public validateProfessor(index, professor, value) {
    if (professor.value == value) {
      if (this.existsProfessorSelecionado(value) === true) {
        this.removeProfessorSelecionado({ item: professor })
      } else {
        this.insertProfessorSelecionado(professor.label, professor.value)
      }
    }
  }

  public existsProfessorSelecionado(value) {
    return this.state.professoresSelecionados.some(professorSelecionado => {
      return professorSelecionado.value == value
    })
  }

  public insertProfessorSelecionado(label, key) {
    const professores = this.state.professoresSelecionados
    professores.push({ label: label, value: key })
    this.setState({ professoresSelecionados: professores.slice() })
  }

  public removeProfessorSelecionado(obj) {
    return this.state.professoresSelecionados.some((professor, index) => {
      if (professor.value === obj.item.value) {
        const itemsChip = this.state.professoresSelecionados
        itemsChip.splice(index, 1)
        this.setState({ professoresSelecionados: itemsChip.slice() })
      }
    })
  }
  public render() {
    return (
      <div>
        <div>
          <label style={this.labelStyle}>
            Como funciona a planilha para convite de PROFESSORES?
          </label>
          <p style={this.pStyle}>
            Com a planilha você pode incluir informações dos PROFESSORES e
            enviar convites de cadastro por e-mail
          </p>
          <p style={this.pStyle}>
            Essa planilha destina-se apenas ao convite de PROFESSORES. Para
            convidar outros cargos, recomendamos o uso do
            <a href="#"> Convite via sistema</a>.
          </p>
          <Button type="primary" onClick={() => {}}>
            Baixar modelo de planilha para convite de professores
          </Button>
        </div>
        <FileUpload />
      </div>
    )
  }
}
