import * as React from 'react'
import { Title, Text } from 'plurall-texts'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import { Checked } from 'plurall-icons'
import { Button } from 'plurall-button'

interface IProps {
  classDataSuccess?: any
  enableInvite?: boolean
  sendInvite?: any
  msgSendInvites?: any
}

interface IState {
  msgSendInvites?: any
}

export default class CreateSuccessClassInviteBodyView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      msgSendInvites: '',
    }
  }

  public bindHandlers() {
    this.handleSendInvite = this.handleSendInvite.bind(this)
  }

  public handleSendInvite() {
    this.setState({ msgSendInvites: 'Convites sendo enviados aguarde...' })
    this.props.sendInvite(true)
  }

  public renderInvite() {
    const inviteData = []
    inviteData.push(
      <div>
        <Row>
          <div style={{ marginTop: '10px', width: '100%' }}>
            <Grid cols="12 12 12 12">
              <Text>
                Opa! identificamos que você preencheu os campos de e-mail da
                planilha anexada. Gostaria de enviar os convites para estes
                usuários (via e-mail)?
              </Text>
            </Grid>
          </div>
        </Row>
        <Row>
          <div style={{ marginTop: '10px', width: '100%' }}>
            <Grid cols="12 12 12 12">
              <div style={{ width: '100%', textAlign: 'center' }}>
                {this.state.msgSendInvites.length <= 0 && (
                  <Button onClick={this.handleSendInvite}>
                    Sim, gostaria de enviar os convites
                  </Button>
                )}
                {this.state.msgSendInvites.length > 0 && (
                  <p>{this.state.msgSendInvites}</p>
                )}
              </div>
            </Grid>
          </div>
        </Row>
      </div>,
    )
    return inviteData
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        <Row>
          <div
            style={{
              marginTop: '20px',
              backgroundColor: '#e7f1f2',
              width: '100%',
              padding: '10px',
            }}
          >
            <Grid cols="12 12 12 12">
              <div style={{ float: 'left', width: '100%' }}>
                <div style={{ float: 'left', width: '6%' }}>
                  <Checked size={45} circle={true} />
                </div>
                <div
                  style={{ float: 'left', width: '90%', marginLeft: '20px' }}
                >
                  <Title color="#2abb9c">Turma(s) criada(s) com sucesso!</Title>
                </div>
              </div>
            </Grid>
            <Grid cols="12 12 12 12">
              <div style={{ float: 'left', padding: '0 40px', width: '100%' }}>
                <ul>
                  <li style={{ listStyle: 'none' }}>
                    <a href="#">{this.props.classDataSuccess.className}</a>
                  </li>
                </ul>
              </div>
            </Grid>
          </div>
        </Row>

        {this.props.enableInvite === true && this.renderInvite()}
        <Row>
          <div style={{ marginTop: '10px', width: '100%' }}>
            <Grid cols="12 12 12 12">
              <Text>
                Ou se preferir, você também pode imprimir os códigos de acesso e
                entregar para seus usuários.
              </Text>
            </Grid>
          </div>
        </Row>

        <Row>
          <div style={{ marginTop: '10px', width: '100%' }}>
            <Grid cols="12 12 12 12">
              <div style={{ width: '100%', textAlign: 'center' }}>
                <Button type="primary" border="#00937E" color="#00937E">
                  Imprimir códigos de acesso
                </Button>
              </div>
            </Grid>
          </div>
        </Row>
      </div>
    )
  }
}
