import * as React from 'react';
import { Text } from 'plurall-texts'
import { Input } from 'plurall-form'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import SchoolStaffPermissions from '../../components/schoolStaff/SchoolStaffPermissions'

interface IState {
  schoolStaff?: any
  professoresSelecionados?: any
  professores?: any
}

interface IProps {
  levels?: any
  items?: any
}

export default class SchoolStaffSystemInviteBodyView extends React.Component<IProps, IState> {

  public levels = [
    {
      label: 'Ensino Infantil',
      value: 'infantil',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Fundamental 1',
      value: 'fund1',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
        { label: 'Portugues', value: 3 },
        { label: 'Matemática', value: 4 },
        { label: 'Portugues', value: 5 },
        { label: 'Matemática', value: 6 },
        { label: 'Portugues', value: 7 },
        { label: 'Matemática', value: 8 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Fundamental 2',
      value: 'fund2',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Ensino Médio',
      value: 'medio',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
    {
      label: 'Pré-vestivular',
      value: 'pre',
      disciplines: [
        { label: 'Portugues', value: 1 },
        { label: 'Matemática', value: 2 },
      ],
      schoolClasses: [
        { label: 'Manhã', value: 1 },
        { label: 'Noite', value: 2 },
      ],
    },
  ]

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return { schoolStaff: { name: '' } }
  }

  public bindHandlers() {
    this.removeProfessorSelecionado = this.removeProfessorSelecionado.bind(this)
    this.handleChangeProfessor = this.handleChangeProfessor.bind(this)
  }

  public handleChangeProfessor(value) {
    if (value !== '') {
      this.state.professores.forEach((professor, index) => {
        this.validateProfessor(index, professor, value)
      })
    }
  }

  public validateProfessor(index, professor, value) {
    if (professor.value == value) {
      if (this.existsProfessorSelecionado(value) === true) {
        this.removeProfessorSelecionado({ item: professor })
      } else {
        this.insertProfessorSelecionado(professor.label, professor.value)
      }
    }
  }

  public existsProfessorSelecionado(value) {
    return this.state.professoresSelecionados.some(professorSelecionado => {
      return professorSelecionado.value == value
    })
  }

  public insertProfessorSelecionado(label, key) {
    const professores = this.state.professoresSelecionados
    professores.push({ label: label, value: key })
    this.setState({ professoresSelecionados: professores.slice() })
  }

  public removeProfessorSelecionado(obj) {
    return this.state.professoresSelecionados.some((professor, index) => {
      if (professor.value === obj.item.value) {
        const itemsChip = this.state.professoresSelecionados
        itemsChip.splice(index, 1)
        this.setState({ professoresSelecionados: itemsChip.slice() })
      }
    })
  }

  public onChangePermissions(permissions) {
    console.log(permissions)
  }

  public render() {
    return (
      <div>
        <Row>
          <Grid cols="12 12 12 12">
            <Text>Nome*</Text>
            <Input
              type="text"
              name="schoolStaffName"
              placeholder="Nome"
              value={this.state.schoolStaff.name}
            />
          </Grid>
        </Row>

        <Row>
          <Grid cols="12 12 6 6">
            <Text>Email*</Text>
            <Input
              type="text"
              name="schoolStaffEmail"
              placeholder="E-mail"
              value={this.state.schoolStaff.email}
            />
          </Grid>
          <Grid cols="12 12 6 6">
            <Text>CPF*</Text>
            <Input
              type="text"
              name="schoolStaffCpf"
              placeholder="___.___.___-__"
              value={this.state.schoolStaff.cpf}
            />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <SchoolStaffPermissions
              levels={this.props.levels}
              onChange={permissions => {
                this.onChangePermissions(permissions)
              }}
            />
          </Grid>
        </Row>
      </div>
    )
  }
}
