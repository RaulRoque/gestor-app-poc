import * as React from 'react';
import { Text } from 'plurall-texts'
import { Input } from 'plurall-form'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import CheckboxGroup from '../../components/common/CheckboxGroup'
import Row from '../../containers/Row'
import SchoolStaffPermissions from '../../components/schoolStaff/SchoolStaffPermissions'

interface IState {
  schoolStaff: any
  professores?: any
  professoresSelecionados?: any
}

export default class SchoolStaffPermissionsBodyView extends React.Component<any, IState> {
  public levels = []

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return { schoolStaff: { name: '' } }
  }

  public bindHandlers() {
    this.removeProfessorSelecionado = this.removeProfessorSelecionado.bind(this)
    this.handleChangeProfessor = this.handleChangeProfessor.bind(this)
  }

  public handleChangeProfessor(value) {
    if (value !== '') {
      this.state.professores.forEach((professor, index) => {
        this.validateProfessor(index, professor, value)
      })
    }
  }

  public validateProfessor(index, professor, value) {
    if (professor.value == value) {
      if (this.existsProfessorSelecionado(value) === true) {
        this.removeProfessorSelecionado({ item: professor })
      } else {
        this.insertProfessorSelecionado(professor.label, professor.value)
      }
    }
  }

  public existsProfessorSelecionado(value) {
    return this.state.professoresSelecionados.some(professorSelecionado => {
      return professorSelecionado.value == value
    })
  }

  public insertProfessorSelecionado(label, key) {
    const professores = this.state.professoresSelecionados
    professores.push({ label: label, value: key })
    this.setState({ professoresSelecionados: professores.slice() })
  }

  public removeProfessorSelecionado(obj) {
    return this.state.professoresSelecionados.some((professor, index) => {
      if (professor.value === obj.item.value) {
        const itemsChip = this.state.professoresSelecionados
        itemsChip.splice(index, 1)
        this.setState({ professoresSelecionados: itemsChip.slice() })
      }
    })
  }

  public onChangePermissions(permissions) {
    console.log(permissions)
  }

  public render() {
    return (
      <div>
        <Row>
          <Grid cols="12 12 12 12">
            <SchoolStaffPermissions
              levels={this.props.levels}
              userPermissions={this.props.userPermissions}
              onChange={permissions => {
                this.onChangePermissions(permissions)
              }}
            />
          </Grid>
        </Row>
      </div>
    )
  }
}
