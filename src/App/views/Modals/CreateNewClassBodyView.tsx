import * as React from 'react'
import { Slug, Text } from 'plurall-texts'
import Grid from '../../containers/Grid'
import CheckListOne from '../../components/common/CheckListOne'
import Row from '../../containers/Row'

interface IProps {
  onChangeCheckOption?: any
}

interface IState {
  checkboxOneSelected?: any
  checkboxOneSelectedes?: any
}

export default class CreateNewClassBodyView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      checkboxOneSelected: [{ value: 1 }],
      checkboxOneSelectedes: [
        {
          value: 1,
          title: 'Incluir alunos via e-mail',
          subTitle: 'Com esta opção você pode:',
          lists: [
            'enviar um convite para o próprio e-mail do aluno, dispensando a necessidade de recortar e imprimir os códigos de acesso.',
            'Para isso, você precisará apenas de uma planilha excel com os NOMES e E-MAILS de cada aluno.',
          ],
          extra: 'Nós recomendamos esta opção! :)',
        },
        {
          value: 2,
          title: 'Incluir alunos via código de acesso nominal',
          subTitle: 'Com esta opção você pode:',
          lists: [
            'imprimir os códigos de acesso com o nome de cada aluno, facilitando a distribuição e o controle do primeiro acesso dos alunos.',
            'Para isso, você precisará apenas de uma planilha com os nomes de cada aluno.',
          ],
        },
        {
          value: 3,
          title: 'Gerar códigos de acesso não nominais',
          lists: [
            'Caso você não tenha uma planilha com os nomes dos alunos, você pode criar códigos de acesso sabendo apenas a quantidade de alunos por turma.',
            'Depois você precisará imprimir, recortar e distribuir os códigos para cada aluno (lembrando que é um código por aluno!)',
          ],
        },
      ],
    }
  }

  public bindHandlers() {
    this.handleChangeCheckListOne = this.handleChangeCheckListOne.bind(this)
  }

  public handleChangeCheckListOne(obj) {
    this.props.onChangeCheckOption(obj)
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        <Row>
          <Grid cols="12 12 12 12">
            <Text color="#F5A623">Importante:</Text>
          </Grid>
          <Grid cols="12 12 12 12">
            <Text>
              Esta decisão pode influenciar a forma como vai entregar os códigos
              de acesso para os alunos!
            </Text>
          </Grid>
          <Grid cols="12 12 12 12">
            <Text>Leia com atenção para fazer sua escolha.</Text>
          </Grid>
          <Grid cols="12 12 12 12">
            <div style={{ width: '100%', marginTop: 20 }}>
              <CheckListOne
                rowCheckbox={true}
                items={this.state.checkboxOneSelectedes}
                itemsChecked={this.state.checkboxOneSelected}
                onChangeCheckListOne={this.handleChangeCheckListOne}
                backgroundActive={'#f7f7f7'}
                background={'#E7F1F2'}
              />
            </div>
          </Grid>
        </Row>
      </div>
    )
  }
}
