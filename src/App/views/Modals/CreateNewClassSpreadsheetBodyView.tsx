import * as React from 'react'
import { Title, Text } from 'plurall-texts'
import { Accordion, PlurallAccordionItem } from 'plurall-accordion'
import Grid from '../../containers/Grid'
import CreateClassForm from '../../components/schoolClasses/CreateClassForm'
import Row from '../../containers/Row'
import { Button } from 'plurall-button'
import Tag from 'plurall-tag'
import { FileUpload } from '../../components/common/FileUpload'
import { CSSProperties } from 'react';

interface IProps {
  onChangeData?: any
  disableSaveButton?: any
}

export default class CreateNewClassSpreadsheetBodyView extends React.Component<IProps> {

  public labelStyle: CSSProperties = {
    display: 'block',
    fontSize: '16px',
    color: '#293A4C',
    fontWeight: 400,
  }
  public pStyle: CSSProperties = {
    display: 'block',
    fontSize: '16px',
    margin: '0 0 10px 0',
    color: '#293A4C',
    fontWeight: 300,
  }

  constructor(props) {
    super(props)
    this.bindHandlers()
  }

  public bindHandlers() {
    this.handleChangeFormDataSave = this.handleChangeFormDataSave.bind(this)
    this.handleChangeFormInvalid = this.handleChangeFormInvalid.bind(this)
  }

  public handleChangeFormDataSave(obj) {
    this.props.onChangeData(obj)
    this.props.disableSaveButton(false)
  }

  public handleChangeFormInvalid() {
    this.props.disableSaveButton(true)
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: '95px' }}>
        <div>
          <label style={this.labelStyle}>
            Como funciona a planilha para inclusão de alunos na turma?
          </label>
          <p style={this.pStyle}>
            Com a planilha, você pode incluir informações dos alunos e enviar
            convites de cadastro pro e-mail
          </p>
          <Button type="primary" onClick={() => {}}>
            Baixar modelo de planilha
          </Button>
        </div>
        <Row>
          <Grid cols="12 12 12 12">
            <div style={{ width: '100%', marginTop: 20, marginBottom: 20 }}>
              <CreateClassForm
                onQtdUsers={false}
                onChangeFormDataClass={this.handleChangeFormDataSave}
                onChangeFormInvalid={this.handleChangeFormInvalid}
                indexClass={0}
              />
            </div>
          </Grid>
        </Row>
        <FileUpload />
      </div>
    )
  }
}
