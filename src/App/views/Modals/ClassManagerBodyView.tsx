import * as React from 'react'
import { Slug, Text } from 'plurall-texts'
import { Input } from 'plurall-form'
import Dropdown from 'plurall-dropdown'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import SchoolClassServices from '../../services/SchoolClassServices'

interface IProps {
  onChangeCheckOption?: any
}

interface IState {
  materiais?: any
  className?: any
  materiaisSelecionados?: any
  turma?: any
}

export default class ClassManagerBodyView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    this.getClass()
  }

  public getInitialState() {
    return {
      materiaisSelecionados: [],
      turma: [],
      className: '',
      materiais: [
        { label: 'Selecione o material', value: '' },
        { label: 'Regular - Sistema Anglo', value: 916 },
        { label: 'Complementar - Sistema Anglo', value: 917 },
        { label: 'Bienal - Sistema Anglo', value: 918 },
        { label: 'Teste - Sistema Anglo', value: 919 },
      ],
    }
  }

  public bindHandlers() {
    this.removeMaterialSelecionado = this.removeMaterialSelecionado.bind(this)
    this.handleChangeMaterial = this.handleChangeMaterial.bind(this)
    this.handleChangeText = this.handleChangeText.bind(this)
  }

  public handleChangeText(e) {
    this.setState({ className: e.target.value })
  }

  public handleChangeMaterial(value) {
    if (value !== '') {
      this.state.materiais.forEach((material, index) => {
        this.validateMaterial(index, material, value)
      })
    }
  }

  public validateMaterial(index, material, value) {
    if (material.value === value) {
      if (this.existsMaterialSelecionado(value) === true) {
        this.removeMaterialSelecionado({ item: material })
      } else {
        this.insertMaterialSelecionado(material.label, material.value)
      }
    }
  }

  public existsMaterialSelecionado(value) {
    return this.state.materiaisSelecionados.some(materialSelecionado => {
      return materialSelecionado.value === value
    })
  }

  public insertMaterialSelecionado(label, key) {
    const materiais = this.state.materiaisSelecionados
    materiais.push({ label: label, value: key })
    this.setState({ materiaisSelecionados: materiais.slice() })
  }

  public removeMaterialSelecionado(obj) {
    return this.state.materiaisSelecionados.some((material, index) => {
      if (material.value === obj.item.value) {
        const itemsChip = this.state.materiaisSelecionados
        itemsChip.splice(index, 1)
        this.setState({ materiaisSelecionados: itemsChip.slice() })
      }
    })
  }

  public async getClassId() {
    const items = await SchoolClassServices.list()
    const description = items['schoolClasses'].map(item => {
      return item
    })
    this.setState({ turma: description[0] })
  }

  public async getClass() {
    await this.getClassId()
    const collectionsClass = this.state.turma.collections.map(item => {
      return { label: item.description, value: item.id }
    })
    this.setState({ className: this.state.turma.schoolClass.description })
    this.setState({ materiaisSelecionados: collectionsClass })
  }

  public render() {
    return (
      <div>
        <Row>
          <Grid cols="12 12 12 12">
            <div style={{ marginBottom: 30, marginTop: 40 }}>
              <Slug>Alterar nome da Turma</Slug>
              <Input
                type="text"
                placeholder="Digite o novo nome da turma"
                onChange={this.handleChangeText}
                value={this.state.className}
              />
            </div>
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 6">
            <Slug>Materiais para: 2018 Ensino Médio 1º ANO</Slug>
            <Text>Materiais *</Text>
            <div style={{ width: '100%', marginBottom: 15 }}>
              <Dropdown
                name="withUsers"
                options={this.state.materiais}
                onChange={this.handleChangeMaterial}
              />
            </div>
          </Grid>
          <Grid cols="12 12 12 12">
            <div style={{ width: '100%', marginBottom: 30 }}>
              <Chip
                items={this.state.materiaisSelecionados}
                onClose={this.removeMaterialSelecionado}
              />
            </div>
          </Grid>
        </Row>
      </div>
    )
  }
}
