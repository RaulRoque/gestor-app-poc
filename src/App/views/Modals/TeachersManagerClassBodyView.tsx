import * as React from 'react';
import { Slug, Text } from 'plurall-texts'
import { Input } from 'plurall-form'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import CheckboxGroup from '../../components/common/CheckboxGroup'
import Row from '../../containers/Row'

interface IState {
  professores?: any
  professoresSelecionados?: any
  dataHeaders?: any
}

export default class TeachersManagerClassBodyView extends React.Component<any, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      professoresSelecionados: [
        { value: 43212, label: 'Bernardo Lobato' },
        { value: 43215, label: 'Danilo Teste' },
      ],
      professores: [
        {
          value: 43212,
          label: 'Bernardo Lobato',
          extra: '(Matemática, Português, Geografia, História)',
        },
        {
          value: 43213,
          label: 'Felipe Oshima',
          extra: '(Matemática, Português, Geografia)',
        },
        {
          value: 43214,
          label: 'Douglas Faria',
          extra: '(Matemática, Português, Geografia, História)',
        },
        {
          value: 43215,
          label: 'Danilo Teste',
          extra: '(Matemática, História)',
        },
      ],
      dataHeaders: ['Selecionar todos'],
    }
  }

  public bindHandlers() {
    this.removeProfessorSelecionado = this.removeProfessorSelecionado.bind(this)
    this.handleChangeProfessor = this.handleChangeProfessor.bind(this)
  }

  public handleChangeProfessor(value) {
    if (value !== '') {
      this.state.professores.forEach((professor, index) => {
        this.validateProfessor(index, professor, value)
      })
    }
  }

  public validateProfessor(index, professor, value) {
    if (professor.value == value) {
      if (this.existsProfessorSelecionado(value) === true) {
        this.removeProfessorSelecionado({ item: professor })
      } else {
        this.insertProfessorSelecionado(professor.label, professor.value)
      }
    }
  }

  public existsProfessorSelecionado(value) {
    return this.state.professoresSelecionados.some(professorSelecionado => {
      return professorSelecionado.value == value
    })
  }

  public insertProfessorSelecionado(label, key) {
    const professores = this.state.professoresSelecionados
    professores.push({ label: label, value: key })
    this.setState({ professoresSelecionados: professores.slice() })
  }

  public removeProfessorSelecionado(obj) {
    return this.state.professoresSelecionados.some((professor, index) => {
      if (professor.value === obj.item.value) {
        const itemsChip = this.state.professoresSelecionados
        itemsChip.splice(index, 1)
        this.setState({ professoresSelecionados: itemsChip.slice() })
      }
    })
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        <Row>
          <Grid cols="12 12 12 12">
            <div style={{ marginTop: 20, marginBottom: 15 }}>
              <Input type="text" placeholder="Buscar Professores" />
            </div>
            <div style={{ width: '100%', marginBottom: 30 }}>
              <Text>Professores selecionados:</Text>
              <Chip
                items={this.state.professoresSelecionados}
                onClose={this.removeProfessorSelecionado}
              />
            </div>
          </Grid>
          <Grid cols="12 12 12 12">
            <Text>Lista de professores disponíveis para essa turma</Text>
            <div style={{ width: '100%', marginTop: 20 }}>
              <CheckboxGroup
                rowCheckbox={true}
                headers={this.state.dataHeaders}
                items={this.state.professores}
                itemsChecked={this.state.professoresSelecionados}
                onChangeCheckBox={this.handleChangeProfessor}
              />
            </div>
          </Grid>
        </Row>
      </div>
    )
  }
}
