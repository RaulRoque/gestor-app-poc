import * as React from 'react'
import { Title, Text } from 'plurall-texts'
import Grid from '../../containers/Grid'
import Row from '../../containers/Row'
import { Checked } from 'plurall-icons'

interface IProps {
  classDataSuccess?: []
}

export default class CreateSuccessClassNominalBodyView extends React.Component<IProps> {
  constructor(props) {
    super(props)
  }

  public renderClasses(item) {
    const classData = []
    classData.push(
      <li style={{ listStyle: 'none', margin: '0 0 10px 0' }}>
        <span>Turma: </span>
        <a href="#">{item.className}</a>
        {item.qtdUsers > 0 &&
          '- (' + item.qtdUsers + ' Código(s) de Acesso Adicionado(s))'}
      </li>,
    )
    return classData
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        <Row>
          <div
            style={{
              marginTop: '20px',
              backgroundColor: '#e7f1f2',
              width: '100%',
              padding: '10px',
            }}
          >
            <Grid cols="12 12 12 12">
              <div style={{ float: 'left', width: '100%' }}>
                <div style={{ float: 'left', width: '6%' }}>
                  <Checked size={45} circle={true} />
                </div>
                <div style={{ float: 'left', width: '94%' }}>
                  <Title color="#2abb9c">Turma(s) criada(s) com sucesso!</Title>
                </div>
              </div>
            </Grid>
            <Grid cols="12 12 12 12">
              <div style={{ float: 'left', marginTop: '15px' }}>
                <ul>
                  {this.props.classDataSuccess.map((item, i) =>
                    this.renderClasses(item),
                  )}
                </ul>
              </div>
            </Grid>
          </div>
        </Row>
      </div>
    )
  }
}
