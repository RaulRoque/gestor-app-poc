import * as React from 'react'
import { Title } from 'plurall-texts'
import Chip from '../../components/common/Chip'

interface IProps {
  usersSelected?: []
  items?: any
}

export default class RemoveUsersBodyView extends React.Component<IProps> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      usersSelected: [],
    }
  }

  public bindHandlers() {
    this.userSelecionado = this.userSelecionado.bind(this)
  }

  public userSelecionado(obj) {
    return this.props.usersSelected.some((user, index) => {
      if (user['value'] === obj.item.value) {
        const userSelectd = this.props.usersSelected
        userSelectd.splice(index, 1)
        this.setState({ usersSelected: userSelectd.slice() })
      }

      return undefined
    })
  }

  public render() {
    return (
      <div>
        <Title size="small">Usuários que serão removidos</Title>
        <Chip items={this.props.usersSelected} onClose={this.userSelecionado} />
      </div>
    )
  }
}
