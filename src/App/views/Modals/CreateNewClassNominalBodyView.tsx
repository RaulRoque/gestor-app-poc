import * as React from 'react'
import { Title, Text } from 'plurall-texts'
import { Accordion, PlurallAccordionItem } from 'plurall-accordion'
import Grid from '../../containers/Grid'
import CreateClassForm from '../../components/schoolClasses/CreateClassForm'
import Row from '../../containers/Row'
import { Button } from 'plurall-button'
import Tag from 'plurall-tag'

interface IProps {
  onChangeData?: any
  disableSaveButton?: any
}

interface IState {
  classDataSave?: any
  classData?: any
  validateButtonDataPush?: any
}

export default class CreateNewClassNominalBodyView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      indexClass: '',
      classData: '',
      classDataSave: [],
      validateButtonDataPush: false,
      invalidForm: [],
    }
  }

  public bindHandlers() {
    this.handleChangeFormDataSave = this.handleChangeFormDataSave.bind(this)
    this.handleChangeDataPush = this.handleChangeDataPush.bind(this)
    this.handleChangeDataEdit = this.handleChangeDataEdit.bind(this)
    this.handleChangeDataRemove = this.handleChangeDataRemove.bind(this)
    this.handleChangeFormInvalid = this.handleChangeFormInvalid.bind(this)
  }

  public handleChangeDataPush() {
    const classDataSave = this.state.classDataSave
    classDataSave.push(this.state.classData)
    this.setState({ classDataSave: classDataSave.slice() })
    this.props.onChangeData(this.state.classDataSave)
    this.props.disableSaveButton(false)
  }

  public handleChangeFormDataSave(obj) {
    if (this.state.classDataSave[obj.indexClass] !== undefined) {
      this.handleChangeDataEdit(obj)
    }
    this.setState({ classData: obj })
    this.setState({ validateButtonDataPush: true })
    const classDataSave = this.state.classDataSave
    let disabled = false
    classDataSave.forEach(e => {
      if (this.validateDisableSaveButton(e) === false) {
        disabled = true
      }
    })
    this.props.disableSaveButton(disabled)
    if (this.state.classDataSave.length === 0) {
      this.props.disableSaveButton(true)
    }
  }

  public validateDisableSaveButton(data) {
    if (
      data.className.length > 0 &&
      data.year.length > 0 &&
      data.level.length > 0 &&
      data.grade.length > 0 &&
      data.collections.length > 0
    ) {
      return true
    } else {
      return false
    }
  }

  public handleChangeDataEdit(classData) {
    const classDataSave = this.state.classDataSave
    classDataSave[classData.indexClass].invalid = classData.invalid
    classDataSave[classData.indexClass].className = classData.className
    classDataSave[classData.indexClass].qtdUsers = classData.qtdUsers
    classDataSave[classData.indexClass].year = classData.year
    classDataSave[classData.indexClass].level = classData.level
    classDataSave[classData.indexClass].grade = classData.grade
    classDataSave[classData.indexClass].collections = classData.collections
    this.setState({ classDataSave: classDataSave.slice() })
    this.props.onChangeData(this.state.classDataSave)
    this.props.disableSaveButton(false)
  }

  public handleChangeDataRemove(i) {
    const classDataSave = this.state.classDataSave
    classDataSave.splice(i, 1)
    this.setState({ classDataSave: classDataSave.slice() })
    this.reorderIndexList()
    this.props.onChangeData(this.state.classDataSave)
    this.props.disableSaveButton(false)
  }

  public handleChangeFormInvalid(classData) {
    if (classData.indexClass === this.state.classDataSave.length) {
      this.setState({ validateButtonDataPush: false })
    }
    if (classData.indexClass !== this.state.classDataSave.length) {
      this.handleChangeDataEdit(classData)
      this.props.disableSaveButton(true)
    }
  }

  public reorderIndexList() {
    const classDataSave = this.state.classDataSave
    for (let i = 0; i < this.state.classDataSave.length; i++) {
      classDataSave[i].indexClass = i
      this.setState({ classDataSave: classDataSave.slice() })
    }
  }

  public renderAccordions(item, i) {
    const classData = []
    let msgError = ''
    if (item.invalid) {
      msgError = ' * Erro'
    }
    classData.push(
      <PlurallAccordionItem title={item.className + msgError}>
        <CreateClassForm
          onChangeFormDataClass={this.handleChangeFormDataSave}
          onChangeFormInvalid={this.handleChangeFormInvalid}
          onQtdUsers={true}
          indexClass={i}
          className={item.className}
          qtdUsers={item.qtdUsers}
          year={item.year}
          level={item.level}
          grade={item.grade}
          collections={item.collections}
        />
        <div style={{ marginTop: '10px', width: '100%', textAlign: 'right' }}>
          <Button
            background="#da544f"
            color="#FFF"
            type="secondary"
            onClick={() => {
              this.handleChangeDataRemove(i)
            }}
          >
            <span>Remover da lista</span>
          </Button>
        </div>
      </PlurallAccordionItem>,
    )
    return classData
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        {this.state.classDataSave.length >= 10 && (
          <Tag
            background="#ff8a65"
            label="Número máximo permitido de turmas é 10."
            bold="900"
          />
        )}
        {this.state.validateButtonDataPush === false &&
          this.state.classDataSave.length < 10 && (
            <Tag
              background="#558bf6"
              label="Para adicionar na lista, preencha todos os campos."
              bold="900"
            />
          )}
        <Row>
          <Grid cols="12 12 12 12">
            <div style={{ width: '100%', marginTop: 20 }}>
              <CreateClassForm
                onQtdUsers={true}
                onChangeFormDataClass={this.handleChangeFormDataSave}
                onChangeFormInvalid={this.handleChangeFormInvalid}
                indexClass={this.state.classDataSave.length}
              />
            </div>
          </Grid>
        </Row>
        <Row>
          <div style={{ marginTop: '20px', width: '100%', textAlign: 'right' }}>
            {this.state.validateButtonDataPush === true &&
            this.state.classDataSave.length < 10 ? (
              <Button type="secondary" onClick={this.handleChangeDataPush}>
                <span> + Adicionar turma </span>
              </Button>
            ) : (
              <span> + Adicionar turma </span>
            )}
          </div>
        </Row>
        <Row>
          <div style={{ marginTop: '20px' }}>
            <Grid cols="12 12 12 12">
              <Title size={'small'}>Lista de turmas que serão criadas:</Title>
              <Text>Lista de turmas que serão criadas:</Text>
            </Grid>
          </div>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <div style={{ marginTop: '10px' }}>
              <Accordion>
                {this.state.classDataSave.map((item, i) =>
                  this.renderAccordions(item, i),
                )}
              </Accordion>
            </div>
          </Grid>
        </Row>
      </div>
    )
  }
}