import * as React from 'react'
import { Text, Slug } from 'plurall-texts'
import Chip from '../../components/common/Chip'
import Grid from '../../containers/Grid'
import CheckboxOne from '../../components/common/CheckboxOne'
import CreateClassForm from '../../components/schoolClasses/CreateClassForm'
import Dropdown from 'plurall-dropdown'
import Row from '../../containers/Row'
import FilterService from '../../services/FilterServices'

interface IProps {
  usersSelected?: []
  items?: any
}

interface IState {
  levels: any
  grades: any
  classes: any
  usersSelected?: any
  selectedYear?: any
  selectedGrade?: any
  selectedOrder?: any
  selectedLevel?: any
  selectedClass?: any
  selectedWithUsers?: any
  checkboxOneSelected?: any
  checkboxOneSelectedes?: any
}

export default class MigrationUserBodyView extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public getInitialState() {
    return {
      checkboxOneSelected: [{ value: 1 }],
      checkboxOneSelectedes: [
        {
          value: 1,
          label: 'Migrar usuários para uma turma já existente',
        },
        {
          value: 2,
          label: 'Migrar usuários para uma nova turma',
        },
      ],
      usersSelected: [],
      selectedYear: '',
      selectedLevel: '',
      selectedGrade: '',
      selectedClass: '',
      levels: [{ label: 'Selecione', value: '' }],
      grades: [{ label: 'Selecione', value: '' }],
      classes: [{ label: 'Selecione', value: '' }],
    }
  }

  public bindHandlers() {
    this.userSelecionado = this.userSelecionado.bind(this)
    this.handleChangeCheck = this.handleChangeCheck.bind(this)
    this.handleChangeYear = this.handleChangeYear.bind(this)
    this.handleChangeLevel = this.handleChangeLevel.bind(this)
    this.handleChangeGrade = this.handleChangeGrade.bind(this)
    this.handleChangeClasses = this.handleChangeClasses.bind(this)
  }

  public userSelecionado(obj) {
    return this.props.usersSelected.some((user, index) => {
      if (user['value'] === obj.item.value) {
        const userSelectd = this.props.usersSelected
        userSelectd.splice(index, 1)
        this.setState({ usersSelected: userSelectd.slice() })
      }

      return undefined
    })
  }

  public handleChangeYear(value) {
    this.setState({ selectedYear: value })
    this.getFilters(value, null)
  }

  public handleChangeLevel(value) {
    this.setState({ selectedLevel: value })
    this.getFilters(null, value)
  }

  public handleChangeGrade(value) {
    this.setState({ selectedGrade: value })
    this.getFilters(null, null, value)
  }

  public handleChangeClasses(value) {
    this.setState({ selectedClass: value })
  }

  public handleChangeCheck(v) {
    const value = parseInt(v)
    this.setState({ checkboxOneSelected: [{ value: value }] })
  }

  public getYears() {
    return [
      { label: 'Ano Letivo', value: '' },
      { label: '2018', value: '2018' },
      { label: '2017', value: '2017' },
      { label: '2016', value: '2016' },
    ]
  }

  public async getFilters(year, level, grade?) {
    if (year) {
      const data = await FilterService.getLevels(year)
      this.setState({ levels: data['levels'] })
    }
    if (level) {
      const data = await FilterService.getGrades(level)
      this.setState({ grades: data['grades'] })
    }
    if (grade) {
      const data = await FilterService.getSchoolClasses()
      this.setState({ classes: data['schoolClasses'] })
    }
  }

  public render() {
    return (
      <div style={{ width: '100%', marginBottom: 50 }}>
        <Row>
          <Grid cols="12 12 12 12">
            <Text>Lista de professores disponíveis para essa turma</Text>
            <div style={{ width: '100%', marginTop: 20 }}>
              <CheckboxOne
                rowCheckbox={true}
                items={this.state.checkboxOneSelectedes}
                itemsChecked={this.state.checkboxOneSelected}
                onChangeCheckBoxOne={this.handleChangeCheck}
                backgroundActive={'#f7f7f7'}
                background={'#f7f7f7'}
              />
            </div>
          </Grid>
        </Row>
        {this.state.checkboxOneSelected[0].value === 1 && (
          <Row>
            <Grid cols="12 12 12 12">
              <div style={{ width: '100%', marginTop: 20 }}>
                <CreateClassForm />
              </div>
            </Grid>
          </Row>
        )}
        {this.state.checkboxOneSelected[0].value === 2 && (
          <Row>
            <Grid cols="12 6 2 2">
              <div style={{ width: '100%', marginTop: '20px' }}>
                <Text>Ano Letivo</Text>
                <Dropdown
                  name="year"
                  options={this.getYears()}
                  onChange={this.handleChangeYear}
                />
              </div>
            </Grid>
            <Grid cols="12 6 3 3">
              <div style={{ width: '100%', marginTop: '20px' }}>
                <Text>Segmento</Text>
                <Dropdown
                  name="levels"
                  options={this.state.levels}
                  onChange={this.handleChangeLevel}
                />
              </div>
            </Grid>
            <Grid cols="12 6 3 3">
              <div style={{ width: '100%', marginTop: '20px' }}>
                <Text>Série</Text>
                <Dropdown
                  name="grades"
                  options={this.state.grades}
                  onChange={this.handleChangeGrade}
                />
              </div>
            </Grid>
            <Grid cols="12 6 3 3">
              <div style={{ width: '100%', marginTop: '20px' }}>
                <Text>Nome da turma</Text>
                <Dropdown
                  name="classes"
                  options={this.state.classes}
                  onChange={this.handleChangeClasses}
                />
              </div>
            </Grid>
          </Row>
        )}
        {this.props.usersSelected.length > 0 && (
          <Row>
            <Grid cols="12 12 12 12">
              <div style={{ width: '100%', marginTop: 20 }}>
                <Slug color="#2ABB9C">Usuários que serão migrados</Slug>
                <Chip
                  items={this.props.usersSelected}
                  onClose={this.userSelecionado}
                />
              </div>
            </Grid>
          </Row>
        )}
      </div>
    )
  }
}
