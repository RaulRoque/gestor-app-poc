import * as React from 'react';

import SchoolClassFilter from '../components/common/filters/SchoolClassFilter'
import SchoolClassServices from '../services/SchoolClassServices'
import DataTable from '../components/common/DataTable'
import Modal from '../components/common/Modal'
import Dropdown from 'plurall-dropdown'
import Grid from '../containers/Grid'
import Row from '../containers/Row'
import Pagination from '../components/common/Pagination'
import { Title } from 'plurall-texts'
import { Button } from 'plurall-button'
import CreateNewClassBodyView from './Modals/CreateNewClassBodyView'
import CreateNewClassNominalBodyView from './Modals/CreateNewClassNominalBodyView'
import CreateNewClassSpreadsheetBodyView from './Modals/CreateNewClassSpreadsheetBodyView'
import CreateSuccessClassNominalBodyView from './Modals/CreateSuccessClassNominalBodyView'
import CreateSuccessClassInviteBodyView from './Modals/CreateSuccessClassInviteBodyView'

import ApolloClient from 'apollo-boost'
import gql from 'graphql-tag'

import { SchoolQuery } from '../graphql/types/graphql-types';

interface IProps {
  history?: any
}

interface IState {
  rows?: any,
  currentPage: any
  dataSaveClass: any
  modalCreateClass: any
  dataHeaders: any
  disableSaveButton: boolean
  modalOptions: any
  filters: any
  totalRecords: any
  sendInviteSuccess: any
}

class SchoolClassView extends React.Component<IProps, IState> {

  public ModalEnum = {
    NEW_CLASS: 0,
    NEW_CLASS_EMAIL: 1,
    NEW_CLASS_EMAIL_SUCCESS: 4,
    NEW_CLASS_NOMINAL: 2,
    NEW_CLASS_NOMINAL_SUCCESS: 5,
    NEW_CLASS_NAO_NOMINAL: 3,
    NEW_CLASS_NAO_NOMINAL_SUCCESS: 6,
  }

  public apiClient = new ApolloClient({
    uri: 'http://cx.local.somosid.com.br/graphql',
  })

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
    Object.freeze(this.ModalEnum)

    this.apiClient
    .query<SchoolQuery.Query>({
      query: gql`
        query buscaEscola {
          school(id: 3644977) {
            legalName
            cnpj
            id
            persons(limit: 3, offset: 1) {
              id
            }
            classes {
              id
              name
              vouchers {
                classId
                voucherCode
                voucherStatusId
                voucherStatus {
                  id
                  name
                }
              }
            }
          }
        }
      `,
    })
    .then(result => {
      console.log(result)
      console.log(1)
    })
  
  }
  public getInitialState() {
    return {
      rows: [],
      currentPage: 1,
      dataSaveClass: '',
      modalCreateClass: 1,
      dataHeaders: [
        'Ano Letivo',
        'Segmento',
        'Série',
        'Material',
        'Turma',
        'Códigos Criados',
        '',
      ],
      disableSaveButton: true,
      modalOptions: undefined,
      filters: undefined,
      totalRecords: undefined,
      sendInviteSuccess: undefined
    }
  }

  public bindHandlers() {
    this.searchSchoolClasses = this.searchSchoolClasses.bind(this)
    this.changePage = this.changePage.bind(this)
    this.changeLocation = this.changeLocation.bind(this)
    this.handleChangeCheckOption = this.handleChangeCheckOption.bind(this)
    this.handleChangeCallbackView = this.handleChangeCallbackView.bind(this)
    this.handleChangeData = this.handleChangeData.bind(this)
    this.handleChangeCallbackSaveClassView = this.handleChangeCallbackSaveClassView.bind(
      this,
    )
    this.handleDisableSaveButton = this.handleDisableSaveButton.bind(this)
    this.handleSendInvite = this.handleSendInvite.bind(this)
  }

  public handleDisableSaveButton(v) {
    this.setState({ disableSaveButton: v })
  }

  public handleChangeCallbackView(v) {
    if (v === 1) {
      this.showModal(this.state.modalCreateClass)
    }
  }

  public handleChangeCheckOption(v) {
    this.setState({ modalCreateClass: parseInt(v) })
  }

  public handleChangeData(obj) {
    this.setState({ dataSaveClass: obj })
  }

  public handleChangeCallbackSaveClassView(v) {
    if (v === 1) {
      console.log('Chamar o serviço para Salvar')
      console.log(this.state.dataSaveClass)
      // Caso o serviço rertorne Success chama o modal de Turma criada com sucesso.
      this.showModal(this.state.modalCreateClass + 3)
    }
  }

  public handleSendInvite(sendInvite) {
    if (sendInvite) {
      console.log('Enviar convite')
    }
  }

  public changeLocation(value) {
    const values = value.split('#')
    switch (values[0]) {
      case 'EMBARQUE':
        this.props.history.push('/turmas/embarque/' + values[1])
        break
      case 'TURMA':
        this.props.history.push('/turmas/gerenciar/' + values[1])
        break
      case 'EXCLUIR':
        this.props.history.push('/some/path')
        break
      default:
        break
    }
  }

  public changePage(page) {
    this.setState({ currentPage: parseInt(page) })
    this.fillTable()
  }

  public searchSchoolClasses() {
    this.fillTable()
  }

  public showModal(opt) {
    this.setState({ modalOptions: opt })
  }

  public closeModal() {
    this.setState({ modalOptions: false })
  }

  public showModalInitial() {
    this.setState({ modalOptions: 0 })
    this.setState({ modalCreateClass: 1 })
  }

  public async fillTable() {
    this.setState({ rows: [] })
    const filters = this.state.filters
    const page = this.state.currentPage
    const items = await SchoolClassServices.list(filters, page)
    const rows = items['schoolClasses'].map(item => {
      return [
        item.period.year,
        item.level.description,
        item.grade.description,
        item.collections.map(c => {
          return c.description
        }),
        '30/0',
        item.schoolClass.description,
        <Dropdown
          key=""
          name="Ano"
          onChange={this.changeLocation}
          options={[
            { label: 'Ações', value: '' },
            { label: 'Gerenciar Embarque de Alunos', value: 'EMBARQUE' },
            { label: 'Gerenciar Turma', value: 'TURMA#' + item.schoolClass.id },
            { label: 'Excluir Turmas', value: 'EXCLUIR' },
          ]}
        />,
      ]
    })
    this.setState({ rows: rows })
    this.setState({ totalRecords: parseInt(items['totalRecords']) })
  }
  public render() {
    return (
      <div>
        <Row>
          <Grid cols="12 12 9 9">
            <Title>Lista de Turmas</Title>
          </Grid>
          <Grid cols="12 12 2 3">
            <div style={{ width: '100%', textAlign: 'right' }}>
              <Button
                color="#FFFFFF"
                background="#2bbbad"
                onClick={() => this.showModalInitial()}
              >
                CRIAR UMA TURMA
              </Button>
            </div>
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <SchoolClassFilter callbackView={this.searchSchoolClasses} />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <DataTable
              items={this.state.rows}
              headers={this.state.dataHeaders}
            />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <Pagination
              totalRecords={this.state.totalRecords}
              recordsPerPage={10}
              currentPage={this.state.currentPage}
              callbackView={this.changePage}
            />
          </Grid>
        </Row>
        <Modal
          show={this.state.modalOptions === this.ModalEnum.NEW_CLASS}
          title="Criar Turma"
          subTitle="Como você quer adicionar os alunos na turma?"
          extras="Passo 1/3"
          width={1000}
          onClose={() => this.closeModal()}
          labelSaveButton="Selecionar"
          callbackView={this.handleChangeCallbackView}
        >
          <div>
            <CreateNewClassBodyView
              onChangeCheckOption={this.handleChangeCheckOption}
            />
          </div>
        </Modal>
        <Modal
          show={this.state.modalOptions === this.ModalEnum.NEW_CLASS_EMAIL}
          title="Criar Turma"
          subTitle="Adiconar alunos via e-mail"
          extras="Passo 2/3"
          width={1000}
          onClose={() => this.closeModal()}
          labelSaveButton="Criar Turma"
          disableSaveButton={this.state.disableSaveButton}
          callbackView={this.handleChangeCallbackSaveClassView}
        >
          <div>
            <CreateNewClassSpreadsheetBodyView
              onChangeData={this.handleChangeData}
              disableSaveButton={this.handleDisableSaveButton}
            />
          </div>
        </Modal>
        <Modal
          show={
            this.state.modalOptions === this.ModalEnum.NEW_CLASS_EMAIL_SUCCESS
          }
          title="Criar Turma"
          width={1000}
          onClose={() => this.closeModal()}
          labelCancelButton={'fechar - ' + this.state.sendInviteSuccess}
        >
          <div>
            <CreateSuccessClassInviteBodyView
              classDataSuccess={this.state.dataSaveClass}
              enableInvite={true}
              sendInvite={this.handleSendInvite}
            />
          </div>
        </Modal>
        <Modal
          show={this.state.modalOptions === this.ModalEnum.NEW_CLASS_NOMINAL}
          title="Criar Turma"
          subTitle="Adiconar alunos via código de acesso nominal"
          extras="Passo 2/3"
          width={1000}
          onClose={() => this.closeModal()}
          labelSaveButton="Criar Turma"
          disableSaveButton={this.state.disableSaveButton}
          callbackView={this.handleChangeCallbackSaveClassView}
        >
          <div>
            <CreateNewClassSpreadsheetBodyView
              onChangeData={this.handleChangeData}
              disableSaveButton={this.handleDisableSaveButton}
            />
          </div>
        </Modal>
        <Modal
          show={
            this.state.modalOptions === this.ModalEnum.NEW_CLASS_NOMINAL_SUCCESS
          }
          title="Criar Turma"
          width={1000}
          onClose={() => this.closeModal()}
          labelCancelButton="Fechar"
        >
          <div>
            <CreateSuccessClassInviteBodyView
              classDataSuccess={this.state.dataSaveClass}
            />
          </div>
        </Modal>
        <Modal
          show={
            this.state.modalOptions === this.ModalEnum.NEW_CLASS_NAO_NOMINAL
          }
          title="Criar Turma"
          subTitle="Adicionar alunos via sistema"
          width={1000}
          onClose={() => this.closeModal()}
          labelSaveButton="Criar Turma"
          disableSaveButton={this.state.disableSaveButton}
          callbackView={this.handleChangeCallbackSaveClassView}
        >
          <div>
            <CreateNewClassNominalBodyView
              onChangeData={this.handleChangeData}
              disableSaveButton={this.handleDisableSaveButton}
            />
          </div>
        </Modal>
        <Modal
          show={
            this.state.modalOptions ===
            this.ModalEnum.NEW_CLASS_NAO_NOMINAL_SUCCESS
          }
          title="Criar Turma"
          width={1000}
          onClose={() => this.closeModal()}
          labelCancelButton="Fechar"
        >
          <div>
            <CreateSuccessClassNominalBodyView
              classDataSuccess={this.state.dataSaveClass}
            />
          </div>
        </Modal>
      </div>
    )
  }
}

export default SchoolClassView
