import * as React from 'react';
import SchoolStaffAllUsersFilter from '../components/common/filters/SchoolStaffAllUsersFilter'
import { Button } from 'plurall-button'
import Modal from '../components/common/Modal'
import Dropdown from 'plurall-dropdown'
import Grid from '../containers/Grid'
import Row from '../containers/Row'
import DataTable from '../components/common/DataTable'
import StudentParentServices from '../services/StudentParentServices'
import Pagination from '../components/common/Pagination'

interface IState {
  modal: any,
  pendingUsers: any,
  rows: any,
  currentPage: any,
  nameUserModal: any,
  emailUserModal: any,
  dataHeaders: any,
  checkedRows?: any,
  totalRecords?: any,
  filters?: any
}

class SchoolStaffAllUsersView extends React.Component<any, IState> {
  public ModalEnum = {
    INVITEUSERPROFS: 1,
    INVITEUSYSTEM: 2,
    PERMISSIONS: 3,
  }

  constructor(props) {
    super(props)
    this.state = this.getInitialState()
    this.bindHandlers()
  }

  public componentDidMount() {
    this.fillTable()
  }

  public getInitialState() {
    return {
      modal: false,
      pendingUsers: 31,
      rows: [],
      currentPage: 1,
      nameUserModal: '',
      emailUserModal: '',
      dataHeaders: [
        'Perfil/Nome',
        'Status',
        'E-mail',
        'Turma',
        'Disciplinas',
        '',
      ],
    }
  }

  public bindHandlers() {
    this.handleChangeInvite = this.handleChangeInvite.bind(this)
    this.changeRow = this.changeRow.bind(this)
    this.changePage = this.changePage.bind(this)
  }

  public handleChangeInvite(value) {
    this.showModal(value)
  }

  public changeRow(value) {
    this.setState({ checkedRows: value })
  }

  public changePage(page) {
    this.setState({ currentPage: parseInt(page) })
    this.fillTable()
  }

  public showModal(modal, name = null, email = null) {
    this.setState({ modal: modal })
    this.setState({ nameUserModal: name })
    this.setState({ emailUserModal: email })
  }

  public closeModal() {
    this.setState({ modal: false })
  }

  public searchSchoolStaffAllUsers() {
    this.fillTable()
  }

  public async fillTable() {
    this.setState({ rows: [] })
    const filters = this.state.filters
    const page = this.state.currentPage
    const items = await StudentParentServices.list(filters, page)
    const rows = items['users'].map(item => {
      return [
        item.name,
        item.status,
        item.email,
        item.schoolClasses.map(c => {
          return c.description
        }),
        item.disciplines.map((c, index) => {
          if (index !== item.disciplines.length - 1) {
            return c.name + ','
          } else {
            return c.name
          }
        }),
        <Button
          key=""
          type="primary"
          onClick={() =>
            this.showModal(this.ModalEnum.PERMISSIONS, item.name, item.email)
          }
        >
          Gerenciar
        </Button>,
      ]
    })
    this.setState({ rows: rows })
    this.setState({ totalRecords: parseInt(items['totalRecords']) })
  }

  public render() {
    return (
      <div>
        <Row>
          <Grid cols="12 12 12 4">
            <Dropdown
              name="invite_users"
              options={[
                { label: 'CONVIDAR EQUIPE ESCOLAR', value: '' },
                {
                  label: 'Convidar via sistema',
                  value: this.ModalEnum.INVITEUSYSTEM,
                },
                {
                  label: 'Convidar professores via planilha',
                  value: this.ModalEnum.INVITEUSERPROFS,
                },
              ]}
              onChange={this.handleChangeInvite}
            />
          </Grid>
        </Row>
        <SchoolStaffAllUsersFilter
          callbackView={() => this.searchSchoolStaffAllUsers()}
        />
        <Row>
          <Grid cols="12 12 12 12">
            <DataTable
              items={this.state.rows}
              headers={this.state.dataHeaders}
            />
          </Grid>
        </Row>
        <Row>
          <Grid cols="12 12 12 12">
            <Pagination
              totalRecords={this.state.totalRecords}
              recordsPerPage={10}
              currentPage={this.state.currentPage}
              callbackView={this.changePage}
            />
          </Grid>
        </Row>
        <Modal
          show={this.state.modal === this.ModalEnum.INVITEUSYSTEM}
          title="Criar usuário - Equipe Escolar"
          subTitle="Defina as permissões desse usuário"
          width={900}
          onClose={() => this.closeModal()}
        />
        <Modal
          show={this.state.modal === this.ModalEnum.INVITEUSERPROFS}
          title="Convidar Professores via Planilha"
          subTitle="Envio de convites para PROFESSORES"
          width={900}
          onClose={() => this.closeModal()}
        />
        <Modal
          show={this.state.modal === this.ModalEnum.PERMISSIONS}
          title="Editar permissões do usuário"
          subTitle={
            'Você está editando o usuário: ' +
            this.state.nameUserModal +
            ' (' +
            this.state.emailUserModal +
            ')'
          }
          width={900}
          onClose={() => this.closeModal()}
        />
      </div>
    )
  }
}

export default SchoolStaffAllUsersView
