import * as React from 'react';
import Tabs from 'plurall-tabs'
import { Title } from 'plurall-texts'
import Wrapper from 'src/App/components/Wrapper'

interface IProps {
  client?: any;
}

interface IState {
  isOpen?: boolean
}

class SchoolStaffView extends React.Component<IProps, IState> {

  constructor(props) {
    super(props)

    this.state = { isOpen: false }
  }
  public toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    })
  }

  public render() {
    return (
      <div>
        <Title>Equipe Escolar</Title>
        <Wrapper>
          <Tabs
            links={[
              {
                label: 'Usuários Pendentes de Aprovação',
                location: '/equipe-escolar/pendentes-aprovacao',
              },
              { label: 'Todos os Usuários', location: '/equipe-escolar/todos' },
            ]}
          />
        </Wrapper>
      </div>
    )
  }
}

export default SchoolStaffView
