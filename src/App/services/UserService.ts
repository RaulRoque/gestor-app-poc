import axios from 'axios';

export default class UserService {
  public static list(filter: any, page: number) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get(`http://localhost:5000/users`)
        .then((response: any) => {
          const ret = response.data
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }
}
