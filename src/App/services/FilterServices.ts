import axios from 'axios';
import SchoolClassFilterModel from '../models/SchoolClassFilterModel'

export default class FilterServices {
  public static getYears() {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get(`https://jsonplaceholder.typicode.com/users`)
        .then((response: any) => {
          const ret = new SchoolClassFilterModel()
          ret.years = response.data
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }

  public static async getLevels(year = null) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get(`http://localhost:5000/levels`)
        .then((response: any) => {
          const ret = new SchoolClassFilterModel()
          ret.levels = response.data.map((i: any) => {
            return { label: i.description, value: i.value }
          })
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }

  public static getGrades(level = null) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get(`http://localhost:5000/grades`)
        .then((response: any) => {
          const ret = new SchoolClassFilterModel()
          ret.grades = response.data.map((i: any) => {
            return { label: i.description, value: i.value }
          })
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }

  public static getSchoolCollections(filters = null) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get('http://localhost:5000/schoolCollections')
        .then((response: any) => {
          const ret = new SchoolClassFilterModel()
          ret.schoolCollections = response.data.map((i: any) => {
            return { label: i.label, value: i.value }
          })
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }

  public static getSchoolClasses(filters = null) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get('http://localhost:5000/schoolClasses')
        .then((response: any) => {
          const ret = new SchoolClassFilterModel()
          ret.schoolClasses = response.data.schoolClasses.map((i: any) => {
            return { label: i.schoolClass.description, value: i.schoolClass.id }
          })
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }
}
