import axios from 'axios';

export default class SchoolClassService {
  public static list(filters?: any, page?: any) {
    return new Promise((resolve: any, reject: any) => {
      axios
        .get(`http://localhost:5000/schoolClasses`)
        .then((response: any) => {
          const ret = response.data
          resolve(ret)
        })
        .catch((error: any) => {
          reject(error)
        })
    })
  }
}
