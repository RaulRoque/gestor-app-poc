import * as React from 'react';
import { Route } from 'react-router-dom'

import SubHeader from 'src/App/components/SubHeader'
import Wrapper from 'src/App/components/Wrapper'
import Layout from 'src/App/containers/Layout'
import SchoolClassView from 'src/App/views/SchoolClassView'
import Header from './components/common/Header'
import SchoolStaffPendingView from './views/SchoolStaffPendingView'
import SchoolStaffView from './views/SchoolStaffView'
import StudentParentView from './views/StudentParentView'

import SchoolClassManagerView from './views/SchoolClassManagerView'
import SchoolStaffAllUsersView from './views/SchoolStaffAllUsersView'

class App extends React.Component {
    public render() {
    return (
      <Layout>
        <div id="app">
          <Header />
          <SubHeader title="Gestor de Turmas" />

          <Wrapper>
            <Route exact={true} path="/turmas" component={SchoolClassView} />
            <Route
              exact={true}
              path="/alunos-e-responsaveis"
              component={StudentParentView}
            />
            <Route
              exact={true}
              path="/turmas/gerenciar/:id"
              component={SchoolClassManagerView}
            />

            <Route path="/equipe-escolar" component={SchoolStaffView} />
            <Route
              exact={true}
              path="/equipe-escolar/todos"
              component={SchoolStaffAllUsersView}
            />
            <Route
              exact={true}
              path="/equipe-escolar/pendentes-aprovacao"
              component={SchoolStaffPendingView}
            />
          </Wrapper>
        </div>
      </Layout>
    )
  }
}

export default App
