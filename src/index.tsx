import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './index.css';
import Router from './Router'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Router />, document.getElementById('root'))
registerServiceWorker();

