import { Redirect } from 'react-router-dom'
import queryString from 'query-string'
import * as React from 'react';

import { getOauthClient } from 'src/utils/oauth'
import { setToken } from 'src/utils/token'

interface IProps {
  location: any
}

interface IState {
  redirect?: any,
}

class LoginCallback extends React.Component<IProps, IState> {

  public componentWillMount() {
    const oauth = getOauthClient(undefined)
    const location = this.props.location
    const fullPath = `${location.pathname}${location.search}${location.hash}`

    oauth.token.getToken(fullPath).then(token => {
      setToken(token.accessToken)
      this.setState({
        redirect: true,
      })
    })
  }

  public render() {
    if (this.state.redirect) {
      const search = queryString.parse(this.props.location.search)
      return <Redirect to={search.redirectTo || '/'} />
    }

    return <div>Você tem que estar logado para acessar esta página</div>
  }
}

export default LoginCallback
